<?php
// ini_set('error_reporting', E_ALL);
// ini_set('display_errors', true);
error_reporting(0);
session_start();
//Load setting file
require_once('setup.php');
//Load database class
require_once(dirname(__FILE__).'/internal_libs/database.class.php');
require_once(dirname(__FILE__).'/internal_libs/common.class.php');
require_once(dirname(__FILE__).'/external_libs/Smarty-3.1.14/libs/SmartyPaginate.class.php');
require_once(dirname(__FILE__).'/external_libs/swiftmailer-5.x/lib/swift_required.php');
require_once(dirname(__FILE__).'/src/index/function_with_database.php');
require_once(dirname(__FILE__).'/src/common/functions_with_database.php');

$common = new common();
//Parameters
#######################################################
$task = $action = $lang = $kwd = null;
if(!empty($_GET['task'])) $task 		= $_GET['task'];
if(!empty($_GET['action'])) $action 	= $_GET['action'];
if(!empty($_GET['kwd'])) $kwd 			= $_GET['kwd'];
//Smarty
$smarty_appform = new PHOTO_SMARTY();
$smarty_appform->assign('smpaginate','index/paginate.tpl');
$smarty_appform->assign('mode', 'index');//problem
$smarty_appform->assign('index_file_name', $index_file_name);
/* Paginate */

if(empty($_SESSION['task'])) $_SESSION['task'] = $task;
if(empty($_SESSION['action'])) $_SESSION['action'] = $action;
if(empty($_SESSION['kwd']))$_SESSION['kwd'] = $kwd;
if ($_SESSION['task'] != $task) SmartyPaginate::disconnect();
if (($_SESSION['task'] === $task) and ($_SESSION['action'] != $action)) SmartyPaginate::disconnect();
if (($_SESSION['task'] === $task) and ($_SESSION['action'] == $action) and $_SESSION['kwd'] != $kwd) SmartyPaginate::disconnect();

SmartyPaginate::disconnect();
SmartyPaginate::connect();
SmartyPaginate::setLimit($limit);


//Post
// $geturl = '?task='.$task.'&action='.$action.'&kwd='.urlencode($kwd);//problem
// SmartyPaginate::setUrl($geturl);
//url of pagination
$urlq =  $_SERVER["QUERY_STRING"];
$urlq = preg_replace('/(&)next=(\d+)/', '', $urlq);
SmartyPaginate::setUrl('?'.$urlq);
/* End Paginate */

//All record
$total_data = null;
#########################################################
/** Error*/
$error = '';
/** Database connection */
$database_connect 		 = new database(DB_HOSTNAME, DB_USER,DB_PASSWORD, DB_DATABASE_NAME);
$database_connect->debug = $debug;
$connected 				 = & $database_connect->_Connect;

if('Post' === $task)
{
	$smarty_appform->display('index/post.tpl');
	exit;
}

if(SmartyPaginate::getCurrentIndex())
{
	$offset = SmartyPaginate::getCurrentIndex();
}
if(SmartyPaginate::getLimit())
{
	$limit = SmartyPaginate::getLimit();
}
//===============================================================================
//task
if ('login' === $task) {
	$smarty_appform->display('index/login.tpl');
	exit;
}

if ('register' === $task) {
	$smarty_appform->display('common/register.tpl');
	exit;
}
if ('user' === $task) {
	$error = array();
	$_SESSION['Staff'] = '';
	if('add' === $action)
	{
		if($_POST)
		{
			$first_name = $common->clean_string($_POST['First_Name']);
			$last_name = $common->clean_string($_POST['Last_Name']);
			$email = $common->clean_string($_POST['E_mail']);
			$re_email = $common->clean_string($_POST['re_email']);
			$gender = $common->clean_string($_POST['gender']);
			$birthday = $common->clean_string($_POST['birthday']);
			$address = $common->clean_string($_POST['Address']);
			$phone_num = $common->clean_string($_POST['Phone_num']);

			if(empty($first_name))$error['First_Name'] = 1;

			if(empty($last_name))$error['Last_Name'] = 1;

			if(empty($email))$error['E_mail'] = 1;
			
			if(empty($re_email))$error['re_email'] = 1;

			if(empty($gender))$error['gender'] = 1;

			if(empty($birthday))$error['birthday'] = 1;

			if(empty($address))$error['Address'] = 1;

			if(empty($phone_num))$error['Phone_num'] = 1;

			if(count($error) == 0)
			{	
				var_dump($bd);
				// create function insert
				addUser_register($first_name,$last_name,$email,$re_email,$gender,$birthday,$address,$phone_num,$gender);
				unset($_SESSION['Staff']);
				header('location:'.$index_file_name.'?task=register');
				exit;
			}
		}
		
		$smarty_appform->display('common/register.tpl');
		exit;
	}
	if('edit' === $action)
	{
		if($_POST)
		{
			$id = $common->clear_string($_POST['id']);
			$first_name = $common->clear_string($_POST['First_Name']);
			$email = $common->clear_string($_POST['Last_Name']);
			$re_email = $common->clean_string($_POST['re_email']);
			$gender = $common->clean_string($_POST['gender']);
			$birthday = $common->clean_string($_POST['birthday']);
			$address = $common->clean_string($_POST['Address']);
			$phone_num = $common->clean_string($_POST['Phone_num']);

			if(empty($first_name))$error['First_Name'] = 1;

			if(empty($last_name))$error['Last_Name'] = 1;

			if(empty($email))$error['E_mail'] = 1;
			
			if(empty($re_email))$error['re_email'] = 1;

			if(empty($gender))$error['gender'] = 1;

			if(empty($birthday))$error['birthday'] = 1;

			if(empty($address))$error['Address'] = 1;

			if(empty($phone_num))$error['Phone_num'] = 1;
			
			if(count($error) == 0)
			{
				updateRegisteraccount($id,$first_name,$last_name,$email,$address,$phone_num);
				header('location:'.$index_file_name.'?task=register');
				exit;
			}
		}
		if(!empty($_GET['id']))
		{
			$result_edit = getRegisterbyID($_GET['id']);
			// var_dump($result_edit);exit;
			$smarty_appform->assign('edit', $result_edit);
			// var_dump($result_edit);exit;
		}
		$smarty_appform->display('common/register.tpl');
		exit;
	}
}

$kwd = '';
if(!empty($_GET['kwd'])) $kwd = $_GET['kwd'];
$results = getAllImage($kwd);
(0 < $total_data) ? SmartyPaginate::setTotal($total_data) : SmartyPaginate::setTotal(1) ;
SmartyPaginate::assign($smarty_appform);;
$smarty_appform->assign('list_images', '');
$smarty_appform->display('index/index.tpl');
exit;
?>
