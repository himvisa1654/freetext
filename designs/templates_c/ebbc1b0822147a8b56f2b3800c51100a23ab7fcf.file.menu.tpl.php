<?php /* Smarty version Smarty-3.1.14, created on 2019-12-16 19:44:00
         compiled from ".\designs\templates\admin\menu.tpl" */ ?>
<?php /*%%SmartyHeaderCode:199545d8c69a604d0c9-49658124%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'ebbc1b0822147a8b56f2b3800c51100a23ab7fcf' => 
    array (
      0 => '.\\designs\\templates\\admin\\menu.tpl',
      1 => 1576500076,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '199545d8c69a604d0c9-49658124',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.14',
  'unifunc' => 'content_5d8c69a60557f7_27634063',
  'variables' => 
  array (
    'admin_file_name' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5d8c69a60557f7_27634063')) {function content_5d8c69a60557f7_27634063($_smarty_tpl) {?><!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="dist/img/Visa.jpg" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p>Visa</p>
          <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
      </div>
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header">MAIN NAVIGATION</li>


        <li class="treeview">
          <a href="#">
            <i class="fas fa-user-edit"></i> <span>User</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?php echo $_smarty_tpl->tpl_vars['admin_file_name']->value;?>
?task=user"><i class="fa fa-users" aria-hidden="true"></i> View All User Account</a></li>
            <li><a href="<?php echo $_smarty_tpl->tpl_vars['admin_file_name']->value;?>
?task=admin_account"><i class="fa fa-cog" aria-hidden="true"></i> View All Staff Account</a></li>
          </ul>
        </li>

        
        <li>
          <a href="<?php echo $_smarty_tpl->tpl_vars['admin_file_name']->value;?>
?task=category">
            <i class="fa fa-folder-open" aria-hidden="true"></i><span>Category</span>
          </a>
        </li>
        
        <li>
          <a href="<?php echo $_smarty_tpl->tpl_vars['admin_file_name']->value;?>
?task=payment">
            <i class="fa fa-money" aria-hidden="true"></i><span>Payment</span>
            <!--<span class="pull-right-container">

            </span>-->
          </a>
        </li>

        <!--<li class="treeview">
          <a href="#">
            <i class="fa fa-table" aria-hidden="true"></i><span>Tables</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="pages/tables/simple.html"><i class="fa fa-circle-o"></i>Simple tables</a></li>
            <li><a href="pages/tables/data.html"><i class="fa fa-circle-o"></i>Data tables</a></li>
          </ul>
        </li>-->


        <!--<li class="treeview">
          <ul class="treeview-menu">
            <li><a href="pages/examples/invoice.html"><i class="fa fa-circle-o"></i> Invoice</a></li>
            <li><a href="pages/examples/profile.html"><i class="fa fa-circle-o"></i> Profile</a></li>
            <li><a href="pages/examples/login.html"><i class="fa fa-circle-o"></i> Login</a></li>
            <li><a href="pages/examples/register.html"><i class="fa fa-circle-o"></i> Register</a></li>
            <li><a href="pages/examples/lockscreen.html"><i class="fa fa-circle-o"></i> Lockscreen</a></li>
            <li><a href="pages/examples/404.html"><i class="fa fa-circle-o"></i> 404 Error</a></li>
            <li><a href="pages/examples/500.html"><i class="fa fa-circle-o"></i> 500 Error</a></li>
            <li><a href="pages/examples/blank.html"><i class="fa fa-circle-o"></i> Blank Page</a></li>
            <li><a href="pages/examples/pace.html"><i class="fa fa-circle-o"></i> Pace Page</a></li>
          </ul>
        </li>-->
        <!--<li class="treeview">
          <a href="#">
            <i class="fa fa-share"></i> <span>Multilevel</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="#"><i class="fa fa-circle-o"></i> Level One</a></li>
            <li class="treeview">
              <a href="#"><i class="fa fa-circle-o"></i> Level One
                <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
              </a>
              <ul class="treeview-menu">
                <li><a href="#"><i class="fa fa-circle-o"></i> Level Two</a></li>
                <li class="treeview">
                  <a href="#"><i class="fa fa-circle-o"></i> Level Two
                    <span class="pull-right-container">
                      <i class="fa fa-angle-left pull-right"></i>
                    </span>
                  </a>
                  <ul class="treeview-menu">
                    <li><a href="#"><i class="fa fa-circle-o"></i> Level Three</a></li>
                    <li><a href="#"><i class="fa fa-circle-o"></i> Level Three</a></li>
                  </ul>
                </li>
              </ul>
            </li>
            <li><a href="#"><i class="fa fa-circle-o"></i> Level One</a></li>
          </ul>
        </li>-->

        <li class="header">LABELS</li>
        <li><a href="#"><i class="fa fa-circle-o text-red"></i> <span>Important</span></a></li>
        <li><a href="#"><i class="fa fa-circle-o text-yellow"></i> <span>Warning</span></a></li>
        <li><a href="#"><i class="fa fa-circle-o text-aqua"></i> <span>Information</span></a></li>
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>
<?php }} ?>