<?php /* Smarty version Smarty-3.1.14, created on 2019-11-27 16:46:51
         compiled from ".\designs\templates\index\post.tpl" */ ?>
<?php /*%%SmartyHeaderCode:120245daadcd08160e1-33237952%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '6701fdec8f20c8fa4a7565f47722fc44f474a5ac' => 
    array (
      0 => '.\\designs\\templates\\index\\post.tpl',
      1 => 1571481240,
      2 => 'file',
    ),
    'a5064f94d595283c27b4f166be3e0ee0165fae0c' => 
    array (
      0 => '.\\designs\\templates\\index\\layout.tpl',
      1 => 1574847930,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '120245daadcd08160e1-33237952',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.14',
  'unifunc' => 'content_5daadcd0816fc6_80542057',
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5daadcd0816fc6_80542057')) {function content_5daadcd0816fc6_80542057($_smarty_tpl) {?><!DOCTYPE html>
<html lang="en">
<head>
  <title> textfree</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="css\list_grid_view.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
</head>
<body>
    <?php echo $_smarty_tpl->getSubTemplate ("index/menu.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

    <div class="container-fluid" style="margin-top:25px;">
        
<div class="jumbotron">
    <div class="container"  style="padding:0px;">
      <div class="panel panel-default">
        <div class="panel-heading">
          <section class="content-header">
            <h2>
              <small>Content Show</small>
            </h2>
            <ol class="breadcrumb">
              <li><a href="<?php echo $_smarty_tpl->tpl_vars['index_file_name']->value;?>
"><i class="fa fa-home" aria-hidden="true"></i></i>Home</a></li>
              <li><i class="fa fa-clipboard" aria-hidden="true"></i>Post</li>
            </ol>
          </section>
        </div>
        <div class="panel-body">
          <form class="form-horizontal" action="index.html" method="post" enctype="multipart/form-data">
            <!--Upload-->
            <div class="form-group">
              <input type="file" name="image/" value="images" class="btn btn-primary">
            </div>
            <!--comment-->
            <div class="form-group">
              <label for="comment">Comment:</label>
              <textarea class="form-control" rows="5" id="comment"></textarea>
            </div>

            <div class="form-group" style="float:right;">
              <input type="submit" name="Submit" value="Submit" class="btn btn-primary">
            </div>
          </form>
        </div>
      </div>
    </div>
</div>

    </div>
  <?php echo $_smarty_tpl->getSubTemplate ("common/footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="//netdna.bootstrapcdn.com/bootstrap/3.0.0/js/bootstrap.min.js"></script>
<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
<script src="js\index.js" charset="utf-8"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/jquery/latest/jquery.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />


</body>
</html>
<?php }} ?>