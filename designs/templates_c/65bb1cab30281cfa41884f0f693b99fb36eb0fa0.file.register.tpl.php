<?php /* Smarty version Smarty-3.1.14, created on 2019-11-27 14:18:38
         compiled from ".\designs\templates\index\register.tpl" */ ?>
<?php /*%%SmartyHeaderCode:175245dad6d3db74889-48789151%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '65bb1cab30281cfa41884f0f693b99fb36eb0fa0' => 
    array (
      0 => '.\\designs\\templates\\index\\register.tpl',
      1 => 1574839062,
      2 => 'file',
    ),
    'a5064f94d595283c27b4f166be3e0ee0165fae0c' => 
    array (
      0 => '.\\designs\\templates\\index\\layout.tpl',
      1 => 1574755781,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '175245dad6d3db74889-48789151',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.14',
  'unifunc' => 'content_5dad6d3dc1a4d3_68646018',
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5dad6d3dc1a4d3_68646018')) {function content_5dad6d3dc1a4d3_68646018($_smarty_tpl) {?><!DOCTYPE html>
<html lang="en">
<head>
  <title> textfree</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="css\list_grid_view.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
</head>
<body>
    <?php echo $_smarty_tpl->getSubTemplate ("index/menu.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

    <div class="container-fluid" style="margin-top:25px;">
        
  <!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">

  <section class="content-header">
      <h1>
        Register
        <small>preview of register for user</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo $_smarty_tpl->tpl_vars['index_file_name']->value;?>
"><i class="fa fa-home" aria-hidden="true"></i></i>Home</a></li>
        <li><a href="<?php echo $_smarty_tpl->tpl_vars['index_file_name']->value;?>
?task=login"><i class="fa fa-user" aria-hidden="true"></i>Login</a></li>
        <li class="active">Register</li>
      </ol>
    </section>
    <div class="container-fluid" style="background-color:#ECF0F1;">
        <form action="<?php echo $_smarty_tpl->tpl_vars['index_file_name']->value;?>
?task=user&amp;action=add" class="form-horizontal" method="POST" style="margin-top:20px">
            <div class="form-group">
                <label for="First_Name" class="control-label col-sm-4">First Name</label>
                <div class="col-sm-5">
                    <span style="color:red;"><?php if ($_smarty_tpl->tpl_vars['error']->value['First_Name']==1){?>Please input field First_Name!<?php }?></span>
                    <input type="text" class="form-control" id="First_Name" name="First_Name" value="<?php echo $_smarty_tpl->tpl_vars['edit']->value['First_Name'];?>
" placeholder="First Name">
                </div>
            </div>
        
            <div class="form-group">
                <label for="Last_Name" class="control-label col-sm-4">Last Name</label>
                <div class="col-sm-5">
                    <span style="color:red;"><?php if ($_smarty_tpl->tpl_vars['error']->value['Last_Name']==1){?>Please input field Last_Name!<?php }?></span>
                    <input type="text" class="form-control" name="Last_Name" value="<?php echo $_smarty_tpl->tpl_vars['edit']->value['Last_Name'];?>
" placeholder="Last Name">
                </div>
            </div>

            <div class="form-group">
                <label for="e_mail" class="control-label col-sm-4">Your E-mail</label>
                <div class="col-sm-5">
                    <input type="text" class="form-control" name="E_mail" value="<?php echo $_smarty_tpl->tpl_vars['edit']->value['email'];?>
" placeholder="E-mail">
                </div>
            </div>  

            <div class="form-group">
                <label for="e_mail" class="control-label col-sm-4">Re-enter E-mail</label>
                <div class="col-sm-5">
                    <input type="text" class="form-control" name="re_email" value="<?php echo $_smarty_tpl->tpl_vars['edit']->value['re_email'];?>
" placeholder="Re-Enter E-mail">
                </div>
            </div>  

            <div class="form-group">
                <label for="gender" class="control-label col-sm-4">Gander</label>
                <div class="col-sm-5">
                  <select class="form-control" name="gender"  id="exampleFormControlSelect1">
                    <option value="1" <?php if ($_smarty_tpl->tpl_vars['edit']->value['gender']==1){?>selected<?php }?>>M</option>
                    <option value="2" <?php if ($_smarty_tpl->tpl_vars['edit']->value['gender']==2){?>selected<?php }?>>F</option>
                    <option value="3" <?php if ($_smarty_tpl->tpl_vars['edit']->value['gender']==3){?>selected<?php }?>>Other</option>
                  </select>
                </div>
            </div>

            <div class="form-group">
                <label for="Last_Name" class="control-label col-sm-4">Date of Birth</label>
                <div class="col-sm-5">
                    <span style="color:red;"><?php if ($_smarty_tpl->tpl_vars['error']->value['First_Name']==1){?>Please input field First_Name!<?php }?></span>
    		        <input type="text" style="box-sizing: border-box;width: 100%;height: 34px;" name="birthday" value=" 10/24/2019" />
                </div>
            </div>

            <div class="form-group">
                <label for="Address" class="control-label col-sm-4">Address</label>
                <div class="col-sm-5">
                    <input type="text" class="form-control" name="Address" value="<?php echo $_smarty_tpl->tpl_vars['edit']->value['address'];?>
" placeholder="Address">
                </div>
            </div>

            <div class="form-group">
                <label for="Phone Number" class="control-label col-sm-4">Phone Number</label>
                <div class="col-sm-5">
                    <input type="text" class="form-control" name="Phone_num" value="<?php echo $_smarty_tpl->tpl_vars['edit']->value['phone_num'];?>
" placeholder="Phone Number">
                </div>
            </div>

            <div class="form-group">
                <div class="col-sm-4" style="float:right">
                    <button name="submit"  id="submit" type="sutmit" class="btn btn-success">save</button>
                    <button type="reset" class="btn btn-danger">Reset</button>
                </div>
            </div>
            
        </form>
    </div>
</div>
  <!-- /.content-wrapper -->

    </div>
  <?php echo $_smarty_tpl->getSubTemplate ("common/footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="//netdna.bootstrapcdn.com/bootstrap/3.0.0/js/bootstrap.min.js"></script>
<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
<script src="js\index.js" charset="utf-8"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/jquery/latest/jquery.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />

<script>
$(function() {
  $('input[name="birthday"]').daterangepicker({
    singleDatePicker: true,
    showDropdowns: true,
    minYear: 1901,
    maxYear: parseInt(moment().format('YYYY'),10)
  }, function(start, end, label) {
    var years = moment().diff(start, 'years');
    alert("You are " + years + " years old!");
  });
});
</script>

</body>
</html>
<?php }} ?>