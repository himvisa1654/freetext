<?php /* Smarty version Smarty-3.1.14, created on 2019-12-16 19:47:22
         compiled from ".\designs\templates\admin\register.tpl" */ ?>
<?php /*%%SmartyHeaderCode:97985d959d3e9d4ac8-37666239%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'fd7386cbd905ebe697401d216864c5322810508c' => 
    array (
      0 => '.\\designs\\templates\\admin\\register.tpl',
      1 => 1574755559,
      2 => 'file',
    ),
    '1b72d09cc80cdd1a6eb26984dfa127e66ea4aac1' => 
    array (
      0 => '.\\designs\\templates\\admin\\layout.tpl',
      1 => 1576500256,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '97985d959d3e9d4ac8-37666239',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.14',
  'unifunc' => 'content_5d959d3eca1ad8_68185290',
  'variables' => 
  array (
    'admin_file_name' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5d959d3eca1ad8_68185290')) {function content_5d959d3eca1ad8_68185290($_smarty_tpl) {?><!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>V-Social Media</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <meta name='viewport' content='width=device-width, initial-scale=1'>
  

  <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
  <!--W3-->
  <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="bower_components/Ionicons/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="dist/css/skins/_all-skins.min.css">
  <!-- Morris chart -->
  <link rel="stylesheet" href="bower_components/morris.js/morris.css">
  <!-- jvectormap -->
  <link rel="stylesheet" href="bower_components/jvectormap/jquery-jvectormap.css">
  <!-- Date Picker -->
  <link rel="stylesheet" href="bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
  <!-- Daterange picker -->
  <link rel="stylesheet" href="bower_components/bootstrap-daterangepicker/daterangepicker.css">
  <!-- bootstrap wysihtml5 - text editor -->
  <link rel="stylesheet" href="plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">
  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="skin-blue sidebar-mini sidebar-mini fixed sidebar-mini-expand-feature sidebar-collapse">
<div class="wrapper">
  <header class="main-header">
    <!-- Logo -->
    <a href="<?php echo $_smarty_tpl->tpl_vars['admin_file_name']->value;?>
" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>V</b>ISA</span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b>HIM VISA</b></span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>

      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          <!-- Messages: style can be found in dropdown.less-->
           
          <!-- Notifications: style can be found in dropdown.less -->
          <li class="dropdown notifications-menu">
            <ul class="dropdown-menu">
              <li>
                <!-- inner menu: contains the actual data -->
                <ul class="menu">
                </ul>
              </li>
            </ul>
          </li>
          <!-- Tasks: style can be found in dropdown.less -->

          <!-- User Account: style can be found in dropdown.less -->
          <li class="dropdown user user-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <img src="dist/img/Visa.jpg" class="user-image" alt="User Image">
              <span class="hidden-xs">Visa</span>
            </a>
            <ul class="dropdown-menu">
              <!-- User image -->
              <li class="user-header">
                <img src="dist/img/Visa.jpg" class="img-circle" alt="User Image">

                <p>
                  Visa - Web Developer
                  <small>E-Khmer Technolog  y</small>
                </p>
              </li>
              <!-- Menu Footer-->
              <li class="user-footer">
                <div class="pull-left">
                  <a href="https://www.facebook.com/him.visa.3?ref=bookmarks" class="btn btn-default btn-flat">Profile</a>
                </div>
                <div class="pull-right">
                  <a href="<?php echo $_smarty_tpl->tpl_vars['admin_file_name']->value;?>
?task=logout" class="btn btn-default btn-flat">Sign out</a>
                </div>
              </li>
            </ul>
          </li>
          <!-- Control Sidebar Toggle Button -->
          <li>
            <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
          </li>
        </ul>
      </div>
    </nav>
  </header>
    <?php if ($_GET['task']!='login'){?>
        <?php echo $_smarty_tpl->getSubTemplate ("admin/menu.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

    <?php }?>
    
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">

  <section class="content-header">
      <h1>
        Register
        <small>preview of register for All Sub Admin_Account</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo $_smarty_tpl->tpl_vars['admin_file_name']->value;?>
"><i class="fa fa-home" aria-hidden="true"></i></i>Home</a></li>
        <li><a href="<?php echo $_smarty_tpl->tpl_vars['admin_file_name']->value;?>
?task=admin_account"><i class="fa fa-user" aria-hidden="true"></i></i>Staff</a></li>
        <li class="active"><i class="fa fa-user" aria-hidden="true"></i>Register</li>
      </ol>
    </section>
    
    <div class="container-fluid" style="background-color:#ECF0F1;">
    <h3>Staff Information</h3>
    <?php if ($_smarty_tpl->tpl_vars['edit']->value['id']){?>
        <form action="<?php echo $_smarty_tpl->tpl_vars['admin_file_name']->value;?>
?task=admin_account&amp;action=edit" class="form-horizontal" method="POST" style="margin-top:20px">
        <input type="hidden" name="id" value="<?php echo $_smarty_tpl->tpl_vars['edit']->value['id'];?>
">
    <?php }else{ ?>
        <form action="<?php echo $_smarty_tpl->tpl_vars['admin_file_name']->value;?>
?task=admin_account&amp;action=add" class="form-horizontal" method="POST" style="margin-top:20px">
    <?php }?>
            <div class="form-group">
                <label for="First_Name" class="control-label col-sm-4">First Name</label>
                <div class="col-sm-5">
                    <span style="color:red;"><?php if ($_smarty_tpl->tpl_vars['error']->value['First_Name']==1){?>Please input field First_Name!<?php }?></span>
                    <input type="text" class="form-control" id="First_Name" name="First_Name" value="<?php echo $_smarty_tpl->tpl_vars['edit']->value['First_Name'];?>
" placeholder="First Name">
                </div>
            </div>
        
            <div class="form-group">
                <label for="Last_Name" class="control-label col-sm-4">Last Name</label>
                <div class="col-sm-5">
                    <span style="color:red;"><?php if ($_smarty_tpl->tpl_vars['error']->value['Last_Name']==1){?>Please input field Last_Name!<?php }?></span>
                    <input type="text" class="form-control" name="Last_Name" value="<?php echo $_smarty_tpl->tpl_vars['edit']->value['Last_Name'];?>
" placeholder="Last Name">
                </div>
            </div>

            <div class="form-group">
                <label for="gender" class="control-label col-sm-4">Gander</label>
                <div class="col-sm-5">
                  <select class="form-control" name="gender"  id="exampleFormControlSelect1">
                    <option value="1" <?php if ($_smarty_tpl->tpl_vars['edit']->value['gender']==1){?>selected<?php }?>>M</option>
                    <option value="2" <?php if ($_smarty_tpl->tpl_vars['edit']->value['gender']==2){?>selected<?php }?>>F</option>
                    <option value="3" <?php if ($_smarty_tpl->tpl_vars['edit']->value['gender']==3){?>selected<?php }?>>Other</option>
                  </select>
                </div>
            </div>

            <div class="form-group">
                <label for="Last_Name" class="control-label col-sm-4">Date of Birth</label>
                <div class="col-sm-5">
                    <span style="color:red;"><?php if ($_smarty_tpl->tpl_vars['error']->value['First_Name']==1){?>Please input field First_Name!<?php }?></span>
    		        <input type="text" style="box-sizing: border-box;width: 100%;height: 34px;" name="birthday" value="10/24/2019" />
                </div>
            </div>

            <div class="form-group">
                <label for="e_mail" class="control-label col-sm-4">E-mail</label>
                <div class="col-sm-5">
                    <input type="text" class="form-control" name="E_mail" value="<?php echo $_smarty_tpl->tpl_vars['edit']->value['email'];?>
" placeholder="E-mail">
                </div>
            </div>  

            <div class="form-group">
                <label for="Address" class="control-label col-sm-4">Address</label>
                <div class="col-sm-5">
                    <input type="text" class="form-control" name="Address" value="<?php echo $_smarty_tpl->tpl_vars['edit']->value['address'];?>
" placeholder="Address">
                </div>
            </div>

            <div class="form-group">
                <label for="Phone Number" class="control-label col-sm-4">Phone Number</label>
                <div class="col-sm-5">
                    <input type="text" class="form-control" name="Phone_num" value="<?php echo $_smarty_tpl->tpl_vars['edit']->value['phone_num'];?>
" placeholder="Phone Number">
                </div>
            </div>
            <hr class="style12">
            <h3>Login Information</h3>

            <div class="form-group">
                <label for="Last_Name" class="control-label col-sm-4">User Name</label>
                <div class="col-sm-5">
                    <span style="color:red;"><?php if ($_smarty_tpl->tpl_vars['error']->value['user_name']==1){?>Please input field user_name!<?php }?></span>
                    <input type="text" class="form-control" name="user_name" value="<?php echo $_smarty_tpl->tpl_vars['edit']->value['user_name'];?>
" placeholder="User Name">
                </div>
            </div>

            <div class="form-group">
                <label for="e_mail" class="control-label col-sm-4">E-mail</label>
                <div class="col-sm-5">
                    <input type="text" class="form-control" name="E_mail" value="<?php echo $_smarty_tpl->tpl_vars['edit']->value['email'];?>
" placeholder="E-mail">
                </div>
            </div>

            <div class="form-group">
                <label for="password" class="control-label col-sm-4">Password</label>
                <div class="col-sm-5">
                    <input type="text" class="form-control" name="password" value="<?php echo $_smarty_tpl->tpl_vars['edit']->value['password'];?>
" placeholder="Password">
                </div>
            </div>

            <div class="form-group">
                <label for="gender" class="control-label col-sm-4">Staff's Category</label>
                <div class="col-sm-5">
                  <select class="form-control" name="staff_category"  id="exampleFormControlSelect1">
                    <option value="1" <?php if ($_smarty_tpl->tpl_vars['edit']->value['staff_category']==1){?>selected<?php }?>>Editor</option>
                    <option value="2" <?php if ($_smarty_tpl->tpl_vars['edit']->value['staff_category']==2){?>selected<?php }?>>Author</option>
                    <option value="3" <?php if ($_smarty_tpl->tpl_vars['edit']->value['staff_category']==3){?>selected<?php }?>>Other</option>
                  </select>
                </div>
            </div>

            <div class="form-group">
                <div class="col-sm-4" style="float:right">
                    <?php if ($_smarty_tpl->tpl_vars['edit']->value['id']){?>
                         <button name="submit"  id="submit" type="sutmit" class="btn btn-success">update</button>
                    <?php }else{ ?>
                        <button name="submit"  id="submit" type="sutmit" class="btn btn-success">save</button>
                        <button type="reset" class="btn btn-danger">Reset</button>
                    <?php }?>
                    
                </div>
            </div>
        </form>
    </div>
    
</div>
  <!-- /.content-wrapper -->

    <?php echo $_smarty_tpl->getSubTemplate ("common/footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

    <!-- Control Sidebar -->
    <aside class="control-sidebar control-sidebar-dark" style="display: none;">
        <!-- Create the tabs -->
        <ul class="nav nav-tabs nav-justified control-sidebar-tabs">
        <li><a href="#control-sidebar-home-tab" data-toggle="tab"><i class="fa fa-home"></i></a></li>
        <li><a href="#control-sidebar-settings-tab" data-toggle="tab"><i class="fa fa-gears"></i></a></li>
        </ul>
        <!-- Tab panes -->
        <div class="tab-content">
        <!-- Home tab content -->
        <div class="tab-pane" id="control-sidebar-home-tab">
            <h3 class="control-sidebar-heading">Recent Activity</h3>
            <ul class="control-sidebar-menu">
            <li>
                <a href="javascript:void(0)">
                <i class="menu-icon fa fa-user bg-yellow"></i>

                <div class="menu-info">
                    <h4 class="control-sidebar-subheading">Visa Updated His Profile</h4>

                    <p>Smart (885)86 623 531</p>
                    <p>Cellcard (855)92 417 122</p>
                </div>
                </a>
            </li>
            <li>
                <a href="javascript:void(0)">
                <i class="menu-icon fa fa-envelope bg-light-blue"></i>

                <div class="menu-info">
                    <h4 class="control-sidebar-subheading">Visa Mailing List</h4>

                    <p>himvisa1654@gmail.com</p>
                </div>
                </a>
            </li>
            </ul>
            <!-- /.control-sidebar-menu -->

        </div>
        <!-- /.tab-pane -->
        <!-- Stats tab content -->
        <div class="tab-pane" id="control-sidebar-stats-tab">Stats Tab Content</div>
        <!-- /.tab-pane -->
        <!-- Settings tab content -->
        <div class="tab-pane" id="control-sidebar-settings-tab">

        </div>
        <!-- /.tab-pane -->
        </div>
    </aside>
    <!-- /.control-sidebar -->
    <!-- Add the sidebar's background. This div must be placed immediately after the control sidebar -->
    <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->
<!-- jQuery 3 -->
<script src="bower_components/jquery/dist/jquery.min.js"></script>
<!-- jQuery UI 1.11.4 -->
<script src="bower_components/jquery-ui/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button);
</script>
<!-- Bootstrap 3.3.7 -->
<script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- Morris.js charts -->
<script src="bower_components/raphael/raphael.min.js"></script>
<script src="bower_components/morris.js/morris.min.js"></script>
<!-- Sparkline -->
<script src="bower_components/jquery-sparkline/dist/jquery.sparkline.min.js"></script>
<!-- jvectormap -->
<script src="plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
<!-- jQuery Knob Chart -->
<script src="bower_components/jquery-knob/dist/jquery.knob.min.js"></script>
<!-- daterangepicker -->
<script src="bower_components/moment/min/moment.min.js"></script>
<script src="bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
<!-- datepicker -->
<script src="bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
<!-- Slimscroll -->
<script src="bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="dist/js/adminlte.min.js"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="dist/js/pages/dashboard.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="dist/js/demo.js"></script>
<script src='https://kit.fontawesome.com/a076d05399.js'></script>
<script src='js/custon_register.js'></script>

<script type="text/javascript" src="https://cdn.jsdelivr.net/jquery/latest/jquery.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />

</body>
</html>

<script>
$(document).ready(function(){
  $("#submit").click(function(){
   var First_Name = $("#First_Name").val();
   var Last_Name  = $("#Last_Name").val();
    validation(First_Name, Last_Name, '', '', '');
  });
});

$(function() {
  $('input[name="birthday"]').daterangepicker({
    singleDatePicker: true,
    showDropdowns: true,
    minYear: 1901,
    maxYear: parseInt(moment().format('YYYY'),10)
  }, function(start, end, label) {
    var years = moment().diff(start, 'years');
    alert("You are " + years + " years old!");
  });
});
   
</script>

</body>

</html>
<?php }} ?>