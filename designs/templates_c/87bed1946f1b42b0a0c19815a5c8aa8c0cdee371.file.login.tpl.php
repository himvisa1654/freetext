<?php /* Smarty version Smarty-3.1.14, created on 2019-12-05 22:29:23
         compiled from ".\designs\templates\admin\login.tpl" */ ?>
<?php /*%%SmartyHeaderCode:242015d8c67dd67a9a8-27028319%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '87bed1946f1b42b0a0c19815a5c8aa8c0cdee371' => 
    array (
      0 => '.\\designs\\templates\\admin\\login.tpl',
      1 => 1575376266,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '242015d8c67dd67a9a8-27028319',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.14',
  'unifunc' => 'content_5d8c67dd6d8967_60027766',
  'variables' => 
  array (
    'admin_file_name' => 0,
    'error' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5d8c67dd6d8967_60027766')) {function content_5d8c67dd6d8967_60027766($_smarty_tpl) {?><!DOCTYPE html>
<html lang="en">
<head>
  <title> textfree</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

</head>
<body>
<div class="container" style="margin-top:10%;background-image: image(image/Eiffel_Tower_at_Night_Paris_Background-1289.jpg);">
  <div class="row">
    <div class="col-lg-offset-3 col-lg-6 col-lg-offset-3">
      <div class="panel panel-primary">
          <div class="panel-heading" style="color:white;background-color:#3498DB;border:red">Admin Login</div>
            <div class="panel-body">
                <form action="<?php echo $_smarty_tpl->tpl_vars['admin_file_name']->value;?>
?task=login" method="post" class="form-login" id="login_box">
                    <?php if ($_smarty_tpl->tpl_vars['error']->value){?>
                    <i class="fa fa-exclamation-triangle" aria-hidden="true"></i> Username or Password is not match.
                    <?php }?>
                    <div class="form-group has-feedback">
                        <input type="text" class="form-control" placeholder="username" name="username" id="username" style="background:#fff;"/>
                        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                    </div>
                    <div class="form-group has-feedback">
                        <input type="password" class="form-control" placeholder="Password" name="password" id="password" />
                        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                    </div>
                    <button type="submit" class="btn btn-primary form-control" style="color:white;background-color:#3498DB;border:red" >Sign In</button>
                </form>
            </div>
      </div>
    </div>
  </div>
</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
<script src='https://kit.fontawesome.com/a076d05399.js'></script>
</body>
</html>
<?php }} ?>