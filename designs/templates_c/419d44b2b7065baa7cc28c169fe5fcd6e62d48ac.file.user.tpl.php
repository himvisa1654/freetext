<?php /* Smarty version Smarty-3.1.14, created on 2020-04-12 16:44:34
         compiled from ".\designs\templates\admin\user.tpl" */ ?>
<?php /*%%SmartyHeaderCode:149835dbbefcd702025-91457359%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '419d44b2b7065baa7cc28c169fe5fcd6e62d48ac' => 
    array (
      0 => '.\\designs\\templates\\admin\\user.tpl',
      1 => 1586684619,
      2 => 'file',
    ),
    '1b72d09cc80cdd1a6eb26984dfa127e66ea4aac1' => 
    array (
      0 => '.\\designs\\templates\\admin\\layout.tpl',
      1 => 1576500256,
      2 => 'file',
    ),
    '3ce4517512b69aff624cf78ee9230afbf0a84755' => 
    array (
      0 => '.\\designs\\templates\\common\\paginate.tpl',
      1 => 1574068807,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '149835dbbefcd702025-91457359',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.14',
  'unifunc' => 'content_5dbbefcd7a55b5_20538264',
  'variables' => 
  array (
    'admin_file_name' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5dbbefcd7a55b5_20538264')) {function content_5dbbefcd7a55b5_20538264($_smarty_tpl) {?><!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>V-Social Media</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <meta name='viewport' content='width=device-width, initial-scale=1'>
  

  <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
  <!--W3-->
  <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="bower_components/Ionicons/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="dist/css/skins/_all-skins.min.css">
  <!-- Morris chart -->
  <link rel="stylesheet" href="bower_components/morris.js/morris.css">
  <!-- jvectormap -->
  <link rel="stylesheet" href="bower_components/jvectormap/jquery-jvectormap.css">
  <!-- Date Picker -->
  <link rel="stylesheet" href="bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
  <!-- Daterange picker -->
  <link rel="stylesheet" href="bower_components/bootstrap-daterangepicker/daterangepicker.css">
  <!-- bootstrap wysihtml5 - text editor -->
  <link rel="stylesheet" href="plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">
  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="skin-blue sidebar-mini sidebar-mini fixed sidebar-mini-expand-feature sidebar-collapse">
<div class="wrapper">
  <header class="main-header">
    <!-- Logo -->
    <a href="<?php echo $_smarty_tpl->tpl_vars['admin_file_name']->value;?>
" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>V</b>ISA</span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b>HIM VISA</b></span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>

      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          <!-- Messages: style can be found in dropdown.less-->
           
          <!-- Notifications: style can be found in dropdown.less -->
          <li class="dropdown notifications-menu">
            <ul class="dropdown-menu">
              <li>
                <!-- inner menu: contains the actual data -->
                <ul class="menu">
                </ul>
              </li>
            </ul>
          </li>
          <!-- Tasks: style can be found in dropdown.less -->

          <!-- User Account: style can be found in dropdown.less -->
          <li class="dropdown user user-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <img src="dist/img/Visa.jpg" class="user-image" alt="User Image">
              <span class="hidden-xs">Visa</span>
            </a>
            <ul class="dropdown-menu">
              <!-- User image -->
              <li class="user-header">
                <img src="dist/img/Visa.jpg" class="img-circle" alt="User Image">

                <p>
                  Visa - Web Developer
                  <small>E-Khmer Technolog  y</small>
                </p>
              </li>
              <!-- Menu Footer-->
              <li class="user-footer">
                <div class="pull-left">
                  <a href="https://www.facebook.com/him.visa.3?ref=bookmarks" class="btn btn-default btn-flat">Profile</a>
                </div>
                <div class="pull-right">
                  <a href="<?php echo $_smarty_tpl->tpl_vars['admin_file_name']->value;?>
?task=logout" class="btn btn-default btn-flat">Sign out</a>
                </div>
              </li>
            </ul>
          </li>
          <!-- Control Sidebar Toggle Button -->
          <li>
            <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
          </li>
        </ul>
      </div>
    </nav>
  </header>
    <?php if ($_GET['task']!='login'){?>
        <?php echo $_smarty_tpl->getSubTemplate ("admin/menu.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

    <?php }?>
    
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        User Account
        <small>preview of All User Account</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo $_smarty_tpl->tpl_vars['admin_file_name']->value;?>
"><i class="fa fa-home" aria-hidden="true"></i></i>Home</a></li>
        <li><a href="#"><i class="fa fa-user" aria-hidden="true"></i>User</a></li>
        <li class="active"><i class="fa fa-cog" aria-hidden="true"></i>User Account</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-md-6">

        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
            <h3 class="box-title"><b>All User Account</b></h3>
              <div class="box-tools pull-right">
                <div class="has-feedback">
                  <input type="text" class="form-control input-sm" placeholder="Search Category">
                  <span class="glyphicon glyphicon-search form-control-feedback"></span>
                </div>
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body table-responsive no-padding">
              <table class="table table-hover">
               <th>N<sup>0</sup> </th>
                    <th>First_Name</th>
                    <th>Last_Name</th>
                    <th>E_mail</th>
                    <th>Re_Email</th>
                    <th>Gender</th>
                    <th>Date of Birth</th>
                    <th>Address</th>
                    <th>phone_num</th>
                    <th>Action</th>
                  </tr>
                  
                     <?php if (COUNT($_smarty_tpl->tpl_vars['list_user_register']->value)>0){?>
                          <?php  $_smarty_tpl->tpl_vars['v'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['v']->_loop = false;
 $_smarty_tpl->tpl_vars['k'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['list_user_register']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['v']->key => $_smarty_tpl->tpl_vars['v']->value){
$_smarty_tpl->tpl_vars['v']->_loop = true;
 $_smarty_tpl->tpl_vars['k']->value = $_smarty_tpl->tpl_vars['v']->key;
?>
                          <tr>
                            <td style="font-size:22px;"><?php echo $_smarty_tpl->tpl_vars['k']->value+1;?>
</td>
                            <td><h4><?php echo $_smarty_tpl->tpl_vars['v']->value['first_name'];?>
</h4></td>
                            <td><h4><?php echo $_smarty_tpl->tpl_vars['v']->value['last_name'];?>
</h4></td>
                            <td><h4><?php echo $_smarty_tpl->tpl_vars['v']->value['email'];?>
</h4></td>
                            <td><h4><?php echo $_smarty_tpl->tpl_vars['v']->value['re_email'];?>
</h4></td>
                            <td><h4><?php if ($_smarty_tpl->tpl_vars['v']->value['gender']==1){?>Male<?php }elseif($_smarty_tpl->tpl_vars['v']->value['gender']==2){?> Female <?php }else{ ?> Other <?php }?></h4></td>
                            <td><h4><?php echo $_smarty_tpl->tpl_vars['v']->value['birthday'];?>
</h4></td>
                            <td><h4><?php echo $_smarty_tpl->tpl_vars['v']->value['address'];?>
</h4></td>
                            <td><h4><?php echo $_smarty_tpl->tpl_vars['v']->value['phone_num'];?>
</h4></td>
                            <td>
                                  <a class="btn btn-warning btn-md" href="<?php echo $_smarty_tpl->tpl_vars['admin_file_name']->value;?>
?task=user&amp;action=edit&amp;id=<?php echo $_smarty_tpl->tpl_vars['v']->value['id'];?>
" ><i class="fa fa-pencil-square-o"></i>EDIT</a>
                                  <button type="button" class="btn btn-danger btn-md" data-toggle="modal" data-target="#myModal_<?php echo $_smarty_tpl->tpl_vars['v']->value['id'];?>
"><i class="fa fa-trash" aria-hidden="true"></i>DELETE</button>
                                  <!-- Modal delete -->
                                  <div class="modal fade" id="myModal_<?php echo $_smarty_tpl->tpl_vars['v']->value['id'];?>
" role="dialog">
                                      <div class="modal-dialog modal-sm">
                                          <div class="modal-content">
                                              <div class="modal-header">
                                                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                  <h4 class="modal-title">Confirmation</h4>
                                              </div>
                                              <div class="modal-body">
                                                  <p>Are you sure to delete?</p>
                                              </div>
                                              <div class="modal-footer">
                                                  <a class="btn btn-warning btn-md" href="<?php echo $_smarty_tpl->tpl_vars['admin_file_name']->value;?>
?task=user&amp;action=del&amp;id=<?php echo $_smarty_tpl->tpl_vars['v']->value['id'];?>
"><i class="fa fa-trash-o"></i>DELETE</a>
                                                  <button type="button" class="btn btn-danger btn-md" data-dismiss="modal"><i class="fa fa-trash" aria-hidden="true"></i> Close</button>
                                              </div>
                                          </div>
                                      </div>
                                  </div>
                                  <button type="button" class="btn btn-primary btn-md" data-toggle="modal" data-target="#largeShoes"><i class="fa fa-pencil-square-o"></i>More</button>
                                  <div class="modal fade" id="largeShoes" tabindex="-1" role="dialog" aria-labelledby="modalLabelLarge" aria-hidden="true">
                                      <div class="modal-dialog modal-lg">
                                          <div class="modal-content">

                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                    </button>
                                                    <h4 class="modal-title" id="modalLabelLarge">Modal Title</h4>
                                                </div>

                                                <div class="modal-body">
                                                    Modal content...
                                                </div>

                                            </div>
                                          </div>
                                      </div>
                            </td>
                        </tr>
                  <?php } ?>
                  <?php }else{ ?>
                        <tr>
                            <td colspan="12" align ="center" style="font-size:16px;"> <i class="fa fa-exclamation-triangle"></i>No category</td>
                        </tr>
                  <?php }?>
              </table>
            </div>
            <!-- /.box-body -->
            

          </div>
          <?php /*  Call merged included template "common/paginate.tpl" */
$_tpl_stack[] = $_smarty_tpl;
 $_smarty_tpl = $_smarty_tpl->setupInlineSubTemplate("common/paginate.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0, '149835dbbefcd702025-91457359');
content_5e92e302ac8701_20406171($_smarty_tpl);
$_smarty_tpl = array_pop($_tpl_stack); /*  End of included template "common/paginate.tpl" */?>
          <!-- /.box -->
        </div>
      </div>
    </section>
    <!-- /.content -->
  </div>
    
    <?php echo $_smarty_tpl->getSubTemplate ("common/footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

    <!-- Control Sidebar -->
    <aside class="control-sidebar control-sidebar-dark" style="display: none;">
        <!-- Create the tabs -->
        <ul class="nav nav-tabs nav-justified control-sidebar-tabs">
        <li><a href="#control-sidebar-home-tab" data-toggle="tab"><i class="fa fa-home"></i></a></li>
        <li><a href="#control-sidebar-settings-tab" data-toggle="tab"><i class="fa fa-gears"></i></a></li>
        </ul>
        <!-- Tab panes -->
        <div class="tab-content">
        <!-- Home tab content -->
        <div class="tab-pane" id="control-sidebar-home-tab">
            <h3 class="control-sidebar-heading">Recent Activity</h3>
            <ul class="control-sidebar-menu">
            <li>
                <a href="javascript:void(0)">
                <i class="menu-icon fa fa-user bg-yellow"></i>

                <div class="menu-info">
                    <h4 class="control-sidebar-subheading">Visa Updated His Profile</h4>

                    <p>Smart (885)86 623 531</p>
                    <p>Cellcard (855)92 417 122</p>
                </div>
                </a>
            </li>
            <li>
                <a href="javascript:void(0)">
                <i class="menu-icon fa fa-envelope bg-light-blue"></i>

                <div class="menu-info">
                    <h4 class="control-sidebar-subheading">Visa Mailing List</h4>

                    <p>himvisa1654@gmail.com</p>
                </div>
                </a>
            </li>
            </ul>
            <!-- /.control-sidebar-menu -->

        </div>
        <!-- /.tab-pane -->
        <!-- Stats tab content -->
        <div class="tab-pane" id="control-sidebar-stats-tab">Stats Tab Content</div>
        <!-- /.tab-pane -->
        <!-- Settings tab content -->
        <div class="tab-pane" id="control-sidebar-settings-tab">

        </div>
        <!-- /.tab-pane -->
        </div>
    </aside>
    <!-- /.control-sidebar -->
    <!-- Add the sidebar's background. This div must be placed immediately after the control sidebar -->
    <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->
<!-- jQuery 3 -->
<script src="bower_components/jquery/dist/jquery.min.js"></script>
<!-- jQuery UI 1.11.4 -->
<script src="bower_components/jquery-ui/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button);
</script>
<!-- Bootstrap 3.3.7 -->
<script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- Morris.js charts -->
<script src="bower_components/raphael/raphael.min.js"></script>
<script src="bower_components/morris.js/morris.min.js"></script>
<!-- Sparkline -->
<script src="bower_components/jquery-sparkline/dist/jquery.sparkline.min.js"></script>
<!-- jvectormap -->
<script src="plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
<!-- jQuery Knob Chart -->
<script src="bower_components/jquery-knob/dist/jquery.knob.min.js"></script>
<!-- daterangepicker -->
<script src="bower_components/moment/min/moment.min.js"></script>
<script src="bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
<!-- datepicker -->
<script src="bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
<!-- Slimscroll -->
<script src="bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="dist/js/adminlte.min.js"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="dist/js/pages/dashboard.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="dist/js/demo.js"></script>
<script src='https://kit.fontawesome.com/a076d05399.js'></script>
<script src='js/custon_register.js'></script>

<script type="text/javascript" src="https://cdn.jsdelivr.net/jquery/latest/jquery.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />

</body>
</html>

    
</body>

</html>
<?php }} ?><?php /* Smarty version Smarty-3.1.14, created on 2020-04-12 16:44:34
         compiled from ".\designs\templates\common\paginate.tpl" */ ?>
<?php if ($_valid && !is_callable('content_5e92e302ac8701_20406171')) {function content_5e92e302ac8701_20406171($_smarty_tpl) {?><?php if (!is_callable('smarty_function_paginate_first')) include 'C:\\wamp64\\www\\freetext\\external_libs\\Smarty-3.1.14\\libs\\plugins\\function.paginate_first.php';
if (!is_callable('smarty_function_paginate_prev')) include 'C:\\wamp64\\www\\freetext\\external_libs\\Smarty-3.1.14\\libs\\plugins\\function.paginate_prev.php';
if (!is_callable('smarty_function_paginate_middle_custom')) include 'C:\\wamp64\\www\\freetext\\external_libs\\Smarty-3.1.14\\libs\\plugins\\function.paginate_middle_custom.php';
if (!is_callable('smarty_function_paginate_next')) include 'C:\\wamp64\\www\\freetext\\external_libs\\Smarty-3.1.14\\libs\\plugins\\function.paginate_next.php';
if (!is_callable('smarty_function_paginate_last')) include 'C:\\wamp64\\www\\freetext\\external_libs\\Smarty-3.1.14\\libs\\plugins\\function.paginate_last.php';
?><?php if ($_smarty_tpl->tpl_vars['paginate']->value['total']>$_smarty_tpl->tpl_vars['paginate']->value['limit']){?><hr style="border-top: 3px solid #eee; border-radius: 4px;"><?php }?>
<div class="col-md-12 text-center">
  <div class="pagination pagination-centered" style="font-weight: 700; margin-bottom: 0px; margin-top: 0;">
    <?php if ($_smarty_tpl->tpl_vars['paginate']->value['total']>$_smarty_tpl->tpl_vars['paginate']->value['limit']){?>
    <ul class="pagination" style="margin:0px; float: left;">
      <?php if ($_smarty_tpl->tpl_vars['paginate']->value['total']>$_smarty_tpl->tpl_vars['paginate']->value['limit']){?>
        <?php if ($_smarty_tpl->tpl_vars['paginate']->value['first']==1){?>
        <li class="disabled"><a data-toggle1="tooltip" data-placement="top" title="Frist Page"><i class='fa fa-angle-left' aria-hidden='true'></i><i class='fa fa-angle-left' aria-hidden='true'></i></a></li>
        <?php }else{ ?>
        <li><?php echo smarty_function_paginate_first(array('text'=>"<i class='fa fa-angle-left' aria-hidden='true'></i><i class='fa fa-angle-left' aria-hidden='true'></i>",'data-toggle1'=>"tooltip",'data-placement'=>"top",'title'=>"Frist Page"),$_smarty_tpl);?>
</li>
        <?php }?>
      <?php }?>

      <li><?php echo smarty_function_paginate_prev(array('text'=>"<i class='fa fa-chevron-left' aria-hidden='true'></i>",'data-toggle1'=>"tooltip",'data-placement'=>"top",'title'=>"Previous Page"),$_smarty_tpl);?>
</li>
      <?php echo smarty_function_paginate_middle_custom(array('page_limit'=>"10",'format'=>"page",'prefix'=>'','suffix'=>'','link_prefix'=>"<li>",'link_suffix'=>"</li>",'active_link_prefix'=>"<li class='active'><a>",'active_link_suffix'=>"</li></a>"),$_smarty_tpl);?>


      <li><?php echo smarty_function_paginate_next(array('text'=>"<i class='fa fa-chevron-right' aria-hidden='true'></i>",'data-toggle1'=>"tooltip",'data-placement'=>"top",'title'=>"Next Page"),$_smarty_tpl);?>
</li>

      <?php if ($_smarty_tpl->tpl_vars['paginate']->value['total']>$_smarty_tpl->tpl_vars['paginate']->value['limit']){?>
        <?php if ($_smarty_tpl->tpl_vars['paginate']->value['total']==$_smarty_tpl->tpl_vars['paginate']->value['last']){?>
        <li class="disabled"><a data-toggle1="tooltip" data-placement="top" title="Last Page"><i class='fa fa-angle-right' aria-hidden='true'></i><i class='fa fa-angle-right' aria-hidden='true'></i></a></li>
        <?php }else{ ?>
        <li><?php echo smarty_function_paginate_last(array('text'=>"<i class='fa fa-angle-right' aria-hidden='true'></i><i class='fa fa-angle-right' aria-hidden='true'></i>",'data-toggle1'=>"tooltip",'data-placement'=>"top",'title'=>"Last Page"),$_smarty_tpl);?>
</li>
        <?php }?>
      <?php }?>
    </ul>

    <div style="margin:0px; margin-left: 10px; float: left; padding: 6px 12px; border: 1px solid; border-radius: 4px; border-color: #ddd;">
      <?php echo $_smarty_tpl->tpl_vars['paginate']->value['total'];?>
/<?php echo $_smarty_tpl->tpl_vars['paginate']->value['page_total'];?>
 Pages
    </div>
    <?php }?>

  </div>
</div>
<?php }} ?>