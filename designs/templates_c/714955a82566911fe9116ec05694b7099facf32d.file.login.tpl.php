<?php /* Smarty version Smarty-3.1.14, created on 2019-11-27 16:46:53
         compiled from ".\designs\templates\index\login.tpl" */ ?>
<?php /*%%SmartyHeaderCode:128935da028a3133db0-96288451%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '714955a82566911fe9116ec05694b7099facf32d' => 
    array (
      0 => '.\\designs\\templates\\index\\login.tpl',
      1 => 1571651025,
      2 => 'file',
    ),
    'a5064f94d595283c27b4f166be3e0ee0165fae0c' => 
    array (
      0 => '.\\designs\\templates\\index\\layout.tpl',
      1 => 1574847930,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '128935da028a3133db0-96288451',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.14',
  'unifunc' => 'content_5da028a31dee62_75305651',
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5da028a31dee62_75305651')) {function content_5da028a31dee62_75305651($_smarty_tpl) {?><!DOCTYPE html>
<html lang="en">
<head>
  <title> textfree</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="css\list_grid_view.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
</head>
<body>
    <?php echo $_smarty_tpl->getSubTemplate ("index/menu.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

    <div class="container-fluid" style="margin-top:25px;">
        
  <div class="content-wrapper">

    <section class="content-header">
        <h1>
          Login
          <small>preview of Login for user</small>
        </h1>
        <ol class="breadcrumb">
          <li><a href="<?php echo $_smarty_tpl->tpl_vars['index_file_name']->value;?>
"><i class="fa fa-home" aria-hidden="true"></i></i>Home</a></li>
          <li class="active"><i class="fa fa-user" aria-hidden="true"></i>Login</li
        </ol>
      </section>
      <div class="container" style="margin-top:5%;background-image: image(image/Eiffel_Tower_at_Night_Paris_Background-1289.jpg);">
        <div class="row">
          <div class="col-lg-offset-3 col-lg-6 col-md-6 col-lg-offset-3">
            <div class="panel panel-primary">
                <div class="panel-heading" style="color:white;background-color:#3498DB;border:red">Admin Login</div>
                  <div class="panel-body">
                      <form action="<?php echo $_smarty_tpl->tpl_vars['admin_file_name']->value;?>
?task=login" method="post" class="form-login" id="login_box">
                          <?php if ($_smarty_tpl->tpl_vars['error']->value){?>
                          <i class="fa fa-exclamation-triangle" aria-hidden="true"></i> Username or Password is not match.
                          <?php }?>
                          <div class="form-group has-feedback">
                              <input type="text" class="form-control" placeholder="username" name="username" id="username" style="background:#fff;"/>
                              <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                          </div>
                          <div class="form-group has-feedback">
                              <input type="password" class="form-control" placeholder="Password" name="password" id="password" />
                              <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                          </div>

                          <div class="form-group has-feedback" style="float:right;">
                            <a href="<?php echo $_smarty_tpl->tpl_vars['index_file_name']->value;?>
?task=register">Forget password ?</a>
                          </div>
                          <div class="form-group">
                            <button type="submit" class="btn btn-primary form-control" style="color:white;background-color:#3498DB;border:red" >Sign In</button>
                          </div>

                          <div class="form-group has-feedback" style="float:right;">
                            <a href="<?php echo $_smarty_tpl->tpl_vars['index_file_name']->value;?>
?task=register">Create Account</a>
                          </div>
                      </form>
                  </div>
            </div>
          </div>
        </div>
      </div>
  </div>


    </div>
  <?php echo $_smarty_tpl->getSubTemplate ("common/footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="//netdna.bootstrapcdn.com/bootstrap/3.0.0/js/bootstrap.min.js"></script>
<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
<script src="js\index.js" charset="utf-8"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/jquery/latest/jquery.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />


</body>
</html>
<?php }} ?>