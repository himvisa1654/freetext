<?php /* Smarty version Smarty-3.1.14, created on 2020-03-02 19:00:13
         compiled from ".\designs\templates\admin\payment.tpl" */ ?>
<?php /*%%SmartyHeaderCode:92055da97483145ea0-97641350%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '812b9b27e184c68a256f1aabee7049e95a42dc21' => 
    array (
      0 => '.\\designs\\templates\\admin\\payment.tpl',
      1 => 1574252997,
      2 => 'file',
    ),
    '1b72d09cc80cdd1a6eb26984dfa127e66ea4aac1' => 
    array (
      0 => '.\\designs\\templates\\admin\\layout.tpl',
      1 => 1576500256,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '92055da97483145ea0-97641350',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.14',
  'unifunc' => 'content_5da9748320bdd1_46400444',
  'variables' => 
  array (
    'admin_file_name' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5da9748320bdd1_46400444')) {function content_5da9748320bdd1_46400444($_smarty_tpl) {?><!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>V-Social Media</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <meta name='viewport' content='width=device-width, initial-scale=1'>
  

  <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
  <!--W3-->
  <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="bower_components/Ionicons/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="dist/css/skins/_all-skins.min.css">
  <!-- Morris chart -->
  <link rel="stylesheet" href="bower_components/morris.js/morris.css">
  <!-- jvectormap -->
  <link rel="stylesheet" href="bower_components/jvectormap/jquery-jvectormap.css">
  <!-- Date Picker -->
  <link rel="stylesheet" href="bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
  <!-- Daterange picker -->
  <link rel="stylesheet" href="bower_components/bootstrap-daterangepicker/daterangepicker.css">
  <!-- bootstrap wysihtml5 - text editor -->
  <link rel="stylesheet" href="plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">
  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="skin-blue sidebar-mini sidebar-mini fixed sidebar-mini-expand-feature sidebar-collapse">
<div class="wrapper">
  <header class="main-header">
    <!-- Logo -->
    <a href="<?php echo $_smarty_tpl->tpl_vars['admin_file_name']->value;?>
" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>V</b>ISA</span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b>HIM VISA</b></span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>

      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          <!-- Messages: style can be found in dropdown.less-->
           
          <!-- Notifications: style can be found in dropdown.less -->
          <li class="dropdown notifications-menu">
            <ul class="dropdown-menu">
              <li>
                <!-- inner menu: contains the actual data -->
                <ul class="menu">
                </ul>
              </li>
            </ul>
          </li>
          <!-- Tasks: style can be found in dropdown.less -->

          <!-- User Account: style can be found in dropdown.less -->
          <li class="dropdown user user-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <img src="dist/img/Visa.jpg" class="user-image" alt="User Image">
              <span class="hidden-xs">Visa</span>
            </a>
            <ul class="dropdown-menu">
              <!-- User image -->
              <li class="user-header">
                <img src="dist/img/Visa.jpg" class="img-circle" alt="User Image">

                <p>
                  Visa - Web Developer
                  <small>E-Khmer Technolog  y</small>
                </p>
              </li>
              <!-- Menu Footer-->
              <li class="user-footer">
                <div class="pull-left">
                  <a href="https://www.facebook.com/him.visa.3?ref=bookmarks" class="btn btn-default btn-flat">Profile</a>
                </div>
                <div class="pull-right">
                  <a href="<?php echo $_smarty_tpl->tpl_vars['admin_file_name']->value;?>
?task=logout" class="btn btn-default btn-flat">Sign out</a>
                </div>
              </li>
            </ul>
          </li>
          <!-- Control Sidebar Toggle Button -->
          <li>
            <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
          </li>
        </ul>
      </div>
    </nav>
  </header>
    <?php if ($_GET['task']!='login'){?>
        <?php echo $_smarty_tpl->getSubTemplate ("admin/menu.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

    <?php }?>
    
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Payment
        <small>preview of All Payments</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo $_smarty_tpl->tpl_vars['admin_file_name']->value;?>
"><i class="fa fa-home" aria-hidden="true"></i></i>Home</a></li>
        <li class="active"><a href="#"><i class="fa fa-paypal" aria-hidden="true"></i>Payment</a></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-md-6">
  
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
            <h3 class="box-title"><b>All Payments</b></h3>
              <div class="box-tools pull-right">
                <div class="has-feedback">
                  <input type="text" class="form-control input-sm" placeholder="Search Payments">
                  <span class="glyphicon glyphicon-search form-control-feedback"></span>
                </div>
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body table-responsive no-padding">
              <table class="table table-hover">
                <tr>
                  <th>No</th>
                  <th>User_Email</th>
                  <th>User_Password</th>
                  <th>Start Date</th>
                  <th>Action</th>
                </tr>
                <tr>
                  <td>183</td>
                  <td>himvisa1654@gmail.com</td>
                  <td>
                    <input class="form-group" type="password" name="password" value="">
                    <input class="form-group" type="checkbox" name="" value="">
                  </td>
                  <td></td>
                  <td><button class="btn btn-warning">Edit</button>
                      <button class="btn btn-danger">Delete</button></td>
                </tr>
                <tr>
                  <td>183</td>
                  <td>himvisa1654@gmail.com</td>
                  <td>
                    <input class="form-group" type="password" name="password" value="">
                    <input class="form-group" type="checkbox" name="" value="">
                  </td>
                  <td></td>
                  <td><button class="btn btn-warning">Edit</button>
                      <button class="btn btn-danger">Delete</button></td>
                </tr>
                <tr>
                  <td>183</td>
                  <td>himvisa1654@gmail.com</td>
                  <td>
                    <input class="form-group" type="password" name="password" value="">
                    <input class="form-group" type="checkbox" name="" value="">
                  </td>
                  <td></td>
                  <td><button class="btn btn-warning">Edit</button>
                      <button class="btn btn-danger">Delete</button></td>
                </tr>
                <tr>
                  <td>175</td>
                  <td>himvisa1654@gmail.com</td>
                  <td>
                    <input class="form-group" type="password" name="password" value="">
                    <input class="form-group" type="checkbox" name="" value="">
                  </td>
                  <td></td>
                  <td><button class="btn btn-warning">Edit</button>
                      <button class="btn btn-danger">Delete</button></td>
                </tr>
              </table>
            </div>
            <!-- /.box-body -->
            
          </div>
          <!--<form class="form-horizontal" action="<?php echo $_smarty_tpl->tpl_vars['admin_file_name']->value;?>
">
            <button type="submit" class="btn btn-danger btn-md">Back</button>
          </form>-->
          <!-- /.box -->
        </div>
      </div>
    </section>
    <!-- /.content -->
  </div>
    
    <?php echo $_smarty_tpl->getSubTemplate ("common/footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

    <!-- Control Sidebar -->
    <aside class="control-sidebar control-sidebar-dark" style="display: none;">
        <!-- Create the tabs -->
        <ul class="nav nav-tabs nav-justified control-sidebar-tabs">
        <li><a href="#control-sidebar-home-tab" data-toggle="tab"><i class="fa fa-home"></i></a></li>
        <li><a href="#control-sidebar-settings-tab" data-toggle="tab"><i class="fa fa-gears"></i></a></li>
        </ul>
        <!-- Tab panes -->
        <div class="tab-content">
        <!-- Home tab content -->
        <div class="tab-pane" id="control-sidebar-home-tab">
            <h3 class="control-sidebar-heading">Recent Activity</h3>
            <ul class="control-sidebar-menu">
            <li>
                <a href="javascript:void(0)">
                <i class="menu-icon fa fa-user bg-yellow"></i>

                <div class="menu-info">
                    <h4 class="control-sidebar-subheading">Visa Updated His Profile</h4>

                    <p>Smart (885)86 623 531</p>
                    <p>Cellcard (855)92 417 122</p>
                </div>
                </a>
            </li>
            <li>
                <a href="javascript:void(0)">
                <i class="menu-icon fa fa-envelope bg-light-blue"></i>

                <div class="menu-info">
                    <h4 class="control-sidebar-subheading">Visa Mailing List</h4>

                    <p>himvisa1654@gmail.com</p>
                </div>
                </a>
            </li>
            </ul>
            <!-- /.control-sidebar-menu -->

        </div>
        <!-- /.tab-pane -->
        <!-- Stats tab content -->
        <div class="tab-pane" id="control-sidebar-stats-tab">Stats Tab Content</div>
        <!-- /.tab-pane -->
        <!-- Settings tab content -->
        <div class="tab-pane" id="control-sidebar-settings-tab">

        </div>
        <!-- /.tab-pane -->
        </div>
    </aside>
    <!-- /.control-sidebar -->
    <!-- Add the sidebar's background. This div must be placed immediately after the control sidebar -->
    <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->
<!-- jQuery 3 -->
<script src="bower_components/jquery/dist/jquery.min.js"></script>
<!-- jQuery UI 1.11.4 -->
<script src="bower_components/jquery-ui/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button);
</script>
<!-- Bootstrap 3.3.7 -->
<script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- Morris.js charts -->
<script src="bower_components/raphael/raphael.min.js"></script>
<script src="bower_components/morris.js/morris.min.js"></script>
<!-- Sparkline -->
<script src="bower_components/jquery-sparkline/dist/jquery.sparkline.min.js"></script>
<!-- jvectormap -->
<script src="plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
<!-- jQuery Knob Chart -->
<script src="bower_components/jquery-knob/dist/jquery.knob.min.js"></script>
<!-- daterangepicker -->
<script src="bower_components/moment/min/moment.min.js"></script>
<script src="bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
<!-- datepicker -->
<script src="bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
<!-- Slimscroll -->
<script src="bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="dist/js/adminlte.min.js"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="dist/js/pages/dashboard.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="dist/js/demo.js"></script>
<script src='https://kit.fontawesome.com/a076d05399.js'></script>
<script src='js/custon_register.js'></script>

<script type="text/javascript" src="https://cdn.jsdelivr.net/jquery/latest/jquery.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />

</body>
</html>

    
</body>

</html>
<?php }} ?>