<ul class="pagination" style="border-top:2px solid #eee; padding-top:5px">
	<li class="waves-effect">{paginate_prev text="<i class='material-icons'>chevron_left</i>"}</li>
	<li class="waves-effect">
		{if $paginate.total gt $paginate.limit}
		{paginate_first text="First Page"}
		{/if}
	</li>
	{if $paginate.total gt $paginate.limit}
	<li class="waves-effect active" style="font-weight:bold;">
		{paginate_middle page_limit="5" prefix="&nbsp;" suffix="&nbsp;" format="page" }
	</li>
	{/if}
	<li class="waves-effect">
		{if $paginate.total gt $paginate.limit}
		{paginate_last text="Last Page"}
		{/if}
	</li>
	<li class="waves-effect">{paginate_next text="<i class='material-icons'>chevron_right</i>"}</li>
	{if $paginate.total gt $paginate.limit}
	{/if}
</ul>
