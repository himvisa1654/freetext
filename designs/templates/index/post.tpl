{extends file="index/layout.tpl"}
{block name="main"}
<div class="jumbotron">
    <div class="container"  style="padding:0px;">
      <div class="panel panel-default">
        <div class="panel-heading">
          <section class="content-header">
            <h2>
              <small>Content Show</small>
            </h2>
            <ol class="breadcrumb">
              <li><a href="{$index_file_name}"><i class="fa fa-home" aria-hidden="true"></i></i>Home</a></li>
              <li><i class="fa fa-clipboard" aria-hidden="true"></i>Post</li>
            </ol>
          </section>
        </div>
        <div class="panel-body">
          <form class="form-horizontal" action="index.html" method="post" enctype="multipart/form-data">
            <!--Upload-->
            <div class="form-group">
              <input type="file" name="image/" value="images" class="btn btn-primary">
            </div>
            <!--comment-->
            <div class="form-group">
              <label for="comment">Comment:</label>
              <textarea class="form-control" rows="5" id="comment"></textarea>
            </div>

            <div class="form-group" style="float:right;">
              <input type="submit" name="Submit" value="Submit" class="btn btn-primary">
            </div>
          </form>
        </div>
      </div>
    </div>
</div>
{/block}
{block name="javascript"}
{/block}
