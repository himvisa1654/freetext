{extends file="index/layout.tpl"}
{include file="css/custom.css" assign=name var1=value}
{block name="main"}
  <div class="content-wrapper">

    <section class="content-header">
        <h1>
          Login
          <small>preview of Login for user</small>
        </h1>
        <ol class="breadcrumb">
          <li><a href="{$index_file_name}"><i class="fa fa-home" aria-hidden="true"></i></i>Home</a></li>
          <li class="active"><i class="fa fa-user" aria-hidden="true"></i>Login</li
        </ol>
      </section>
      <div class="container" style="margin-top:5%;background-image: image(image/Eiffel_Tower_at_Night_Paris_Background-1289.jpg);">
        <div class="row">
          <div class="col-lg-offset-3 col-lg-6 col-md-6 col-lg-offset-3">
            <div class="panel panel-primary">
                <div class="panel-heading" style="color:white;background-color:#3498DB;border:red">Admin Login</div>
                  <div class="panel-body">
                      <form action="{$admin_file_name}?task=login" method="post" class="form-login" id="login_box">
                          {if $error }
                          <i class="fa fa-exclamation-triangle" aria-hidden="true"></i> Username or Password is not match.
                          {/if}
                          <div class="form-group has-feedback">
                              <input type="text" class="form-control" placeholder="username" name="username" id="username" style="background:#fff;"/>
                              <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                          </div>
                          <div class="form-group has-feedback">
                              <input type="password" class="form-control" placeholder="Password" name="password" id="password" />
                              <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                          </div>

                          <div class="form-group has-feedback" style="float:right;">
                            <a href="{$index_file_name}?task=register">Forget password ?</a>
                          </div>
                          <div class="form-group">
                            <button type="submit" class="btn btn-primary form-control" style="color:white;background-color:#3498DB;border:red" >Sign In</button>
                          </div>

                          <div class="form-group has-feedback" style="float:right;">
                            <a href="{$index_file_name}?task=register">Create Account</a>
                          </div>
                      </form>
                  </div>
            </div>
          </div>
        </div>
      </div>
  </div>

{/block}
{block name="javascript"}
{/block}
