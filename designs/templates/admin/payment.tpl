{extends file="admin/layout.tpl"}
{block name="main"}
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Payment
        <small>preview of All Payments</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="{$admin_file_name}"><i class="fa fa-home" aria-hidden="true"></i></i>Home</a></li>
        <li class="active"><a href="#"><i class="fa fa-paypal" aria-hidden="true"></i>Payment</a></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-md-6">
  
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
            <h3 class="box-title"><b>All Payments</b></h3>
              <div class="box-tools pull-right">
                <div class="has-feedback">
                  <input type="text" class="form-control input-sm" placeholder="Search Payments">
                  <span class="glyphicon glyphicon-search form-control-feedback"></span>
                </div>
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body table-responsive no-padding">
              <table class="table table-hover">
                <tr>
                  <th>No</th>
                  <th>User_Email</th>
                  <th>User_Password</th>
                  <th>Start Date</th>
                  <th>Action</th>
                </tr>
                <tr>
                  <td>183</td>
                  <td>himvisa1654@gmail.com</td>
                  <td>
                    <input class="form-group" type="password" name="password" value="">
                    <input class="form-group" type="checkbox" name="" value="">
                  </td>
                  <td></td>
                  <td><button class="btn btn-warning">Edit</button>
                      <button class="btn btn-danger">Delete</button></td>
                </tr>
                <tr>
                  <td>183</td>
                  <td>himvisa1654@gmail.com</td>
                  <td>
                    <input class="form-group" type="password" name="password" value="">
                    <input class="form-group" type="checkbox" name="" value="">
                  </td>
                  <td></td>
                  <td><button class="btn btn-warning">Edit</button>
                      <button class="btn btn-danger">Delete</button></td>
                </tr>
                <tr>
                  <td>183</td>
                  <td>himvisa1654@gmail.com</td>
                  <td>
                    <input class="form-group" type="password" name="password" value="">
                    <input class="form-group" type="checkbox" name="" value="">
                  </td>
                  <td></td>
                  <td><button class="btn btn-warning">Edit</button>
                      <button class="btn btn-danger">Delete</button></td>
                </tr>
                <tr>
                  <td>175</td>
                  <td>himvisa1654@gmail.com</td>
                  <td>
                    <input class="form-group" type="password" name="password" value="">
                    <input class="form-group" type="checkbox" name="" value="">
                  </td>
                  <td></td>
                  <td><button class="btn btn-warning">Edit</button>
                      <button class="btn btn-danger">Delete</button></td>
                </tr>
              </table>
            </div>
            <!-- /.box-body -->
            
          </div>
          <!--<form class="form-horizontal" action="{$admin_file_name}">
            <button type="submit" class="btn btn-danger btn-md">Back</button>
          </form>-->
          <!-- /.box -->
        </div>
      </div>
    </section>
    <!-- /.content -->
  </div>
    {/block}
        {block name="javascript"}
    {/block}