<!DOCTYPE html>
<html lang="en">
<head>
  <title> textfree</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

</head>
<body>
<div class="container" style="margin-top:10%;background-image: image(image/Eiffel_Tower_at_Night_Paris_Background-1289.jpg);">
  <div class="row">
    <div class="col-lg-offset-3 col-lg-6 col-lg-offset-3">
      <div class="panel panel-primary">
          <div class="panel-heading" style="color:white;background-color:#3498DB;border:red">Admin Login</div>
            <div class="panel-body">
                <form action="{$admin_file_name}?task=login" method="post" class="form-login" id="login_box">
                    {if $error }
                    <i class="fa fa-exclamation-triangle" aria-hidden="true"></i> Username or Password is not match.
                    {/if}
                    <div class="form-group has-feedback">
                        <input type="text" class="form-control" placeholder="username" name="username" id="username" style="background:#fff;"/>
                        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                    </div>
                    <div class="form-group has-feedback">
                        <input type="password" class="form-control" placeholder="Password" name="password" id="password" />
                        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                    </div>
                    <button type="submit" class="btn btn-primary form-control" style="color:white;background-color:#3498DB;border:red" >Sign In</button>
                </form>
            </div>
      </div>
    </div>
  </div>
</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
<script src='https://kit.fontawesome.com/a076d05399.js'></script>
</body>
</html>
