{extends file="admin/layout.tpl"}
{block name="main"}
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        View all Staff Account
        <small>preview of View all Staff Account</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="{$admin_file_name}"><i class="fa fa-home" aria-hidden="true"></i></i>Home</a></li>
        <li class="active"><i class="fa fa-user" aria-hidden="true"></i>User</a></li>
        <li class="active"><i class="fa fa-cog" aria-hidden="true"></i>View all Staff Account</li>
      </ol>
    </section>
    <!-- Main content -->
      <section class="content">
        <div class="row">
          <div class="col-md-6">
            <a href="{$admin_file_name}?task=admin_account&amp;action=add" class="btn btn-danger">Add New Staff</a>
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
        <div class="row">
          <div class="col-xs-12">
            <div class="box">
              <div class="box-header">
                <h3 class="box-title">All User Setting</h3>
                  <div class="box-tools pull-right">
                    <div class="has-feedback">
                      <form class="form-inline" action="{$admin_file_name}" method="get">
                        <input type="hidden" name="task" value="admin_account">
                        <input type="text" class="form-control input-sm" placeholder="Search Category" name="kwd" value="{$smarty.get.kwd}">
                      <button class="btn btn-sm btn-info" type="submit"><i class="fa fa-search"></i></button>
                    </div>
                  </div>
                </form>
              </div>
              <!-- /.box-header -->
              <div class="box-body table-responsive no-padding">
                <table class="table table-hover">
                  <tr>
                    <th>N<sup>0</sup> </th>
                    <th>First_Name</th>
                    <th>Last_Name</th>
                    <th>User Name</th>
                    <th>E_mail</th>
                    <th>Password</th>
                    <th>Gender</th>
                    <th>Date of Birth</th>
                    <th>Staff Category</th>
                    <th>Address</th>
                    <th>Phone_num</th>
                    <th>Action</th>
                  </tr>
                     {if COUNT($listRegister) > 0}
                          {foreach from = $listRegister item = v key=k}
                          <tr>
                            <td style="font-size:22px;">{$k+1}</td>
                            <td><h4>{$v.First_Name}</h4></td>
                            <td><h4>{$v.Last_Name}</h4></td>
                            <td><h4>{$v.user_name}</h4></td>
                            <td><h4>{$v.email}</h4></td>
                            <td><h4>{$v.password}</h4></td>
                            <td><h4>{if $v.gender eq 1}Male{elseif $v.gender eq 2} Female {else} Other {/if}</h4></td>
                            <td><h4>{$v.birthday}</h4></td>
                            <td><h4>{if $v.staff_category eq 1}Editor{else if $v.staff_category eq 2}Author{else}Other{/if}</h4></td>
                            <td><h4>{$v.address}</h4></td>
                            <td><h4>{$v.phone_num}</h4></td>
                            <td>
                                  <a class="btn btn-warning btn-md" href="{$admin_file_name}?task=admin_account&amp;action=edit&amp;id={$v.id}" ><i class="fa fa-pencil-square-o"></i>EDIT</a>
                                  <button type="button" class="btn btn-danger btn-md" data-toggle="modal" data-target="#myModal_{$v.id}"><i class="fa fa-trash" aria-hidden="true"></i>DELETE</button>
                                  <!-- Modal delete -->
                                  <div class="modal fade" id="myModal_{$v.id}" role="dialog">
                                      <div class="modal-dialog modal-sm">
                                          <div class="modal-content">
                                              <div class="modal-header">
                                                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                  <h4 class="modal-title">Confirmation</h4>
                                              </div>
                                              <div class="modal-body">
                                                  <p>Are you sure to delete?</p>
                                              </div>
                                              <div class="modal-footer">
                                                  <a class="btn btn-warning btn-md" href="{$admin_file_name}?task=admin_account&amp;action=del&amp;id={$v.id}"><i class="fa fa-trash-o"></i>DELETE</a>
                                                  <button type="button" class="btn btn-danger btn-md" data-dismiss="modal"><i class="fa fa-trash" aria-hidden="true"></i> Close</button>
                                              </div>
                                          </div>
                                      </div>
                                  </div>
                            </td>
                        </tr>
                  {/foreach}
                  {else}
                        <tr>
                            <td colspan="12" align ="center" style="font-size:16px;"><i class="fa fa-exclamation-triangle"></i>No category</td>
                        </tr>
                  {/if}
                </table>
                
              </div>
              <!-- /.box-body -->
              
            </div>
            {include file="common/paginate.tpl"}
            <!-- /.box -->
          </div>
          
        </div>
      </section>
    <!-- /.content -->
    
  </div>
  
    {/block}
        {block name="javascript"}
    {/block}
