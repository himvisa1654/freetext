{extends file="admin/layout.tpl"}
{include file="css/line_css.css"}
{include file="internal_libs/validate.class" assign=name var1=value}
{block name="main"}
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">

  <section class="content-header">
      <h1>
        Register
        <small>preview of register for All Sub Admin_Account</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="{$admin_file_name}"><i class="fa fa-home" aria-hidden="true"></i></i>Home</a></li>
        <li><a href="{$admin_file_name}?task=admin_account"><i class="fa fa-user" aria-hidden="true"></i></i>Staff</a></li>
        <li class="active"><i class="fa fa-user" aria-hidden="true"></i>Register</li>
      </ol>
    </section>
    
    <div class="container-fluid" style="background-color:#ECF0F1;">
    <h3>Staff Information</h3>
    {if $edit.id}
        <form action="{$admin_file_name}?task=admin_account&amp;action=edit" class="form-horizontal" method="POST" style="margin-top:20px">
        <input type="hidden" name="id" value="{$edit.id}">
    {else}
        <form action="{$admin_file_name}?task=admin_account&amp;action=add" class="form-horizontal" method="POST" style="margin-top:20px">
    {/if}
            <div class="form-group">
                <label for="First_Name" class="control-label col-sm-4">First Name</label>
                <div class="col-sm-5">
                    <span style="color:red;">{if $error.First_Name eq 1}Please input field First_Name!{/if}</span>
                    <input type="text" class="form-control" id="First_Name" name="First_Name" value="{$edit.First_Name}" placeholder="First Name">
                </div>
            </div>
        
            <div class="form-group">
                <label for="Last_Name" class="control-label col-sm-4">Last Name</label>
                <div class="col-sm-5">
                    <span style="color:red;">{if $error.Last_Name eq 1}Please input field Last_Name!{/if}</span>
                    <input type="text" class="form-control" name="Last_Name" value="{$edit.Last_Name}" placeholder="Last Name">
                </div>
            </div>

            <div class="form-group">
                <label for="gender" class="control-label col-sm-4">Gander</label>
                <div class="col-sm-5">
                  <select class="form-control" name="gender"  id="exampleFormControlSelect1">
                    <option value="1" {if $edit.gender eq 1}selected{/if}>M</option>
                    <option value="2" {if $edit.gender eq 2}selected{/if}>F</option>
                    <option value="3" {if $edit.gender eq 3}selected{/if}>Other</option>
                  </select>
                </div>
            </div>

            <div class="form-group">
                <label for="Last_Name" class="control-label col-sm-4">Date of Birth</label>
                <div class="col-sm-5">
                    <span style="color:red;">{if $error.First_Name eq 1}Please input field First_Name!{/if}</span>
    		        <input type="text" style="box-sizing: border-box;width: 100%;height: 34px;" name="birthday" value="10/24/2019" />
                </div>
            </div>

            <div class="form-group">
                <label for="e_mail" class="control-label col-sm-4">E-mail</label>
                <div class="col-sm-5">
                    <input type="text" class="form-control" name="E_mail" value="{$edit.email}" placeholder="E-mail">
                </div>
            </div>  

            <div class="form-group">
                <label for="Address" class="control-label col-sm-4">Address</label>
                <div class="col-sm-5">
                    <input type="text" class="form-control" name="Address" value="{$edit.address}" placeholder="Address">
                </div>
            </div>

            <div class="form-group">
                <label for="Phone Number" class="control-label col-sm-4">Phone Number</label>
                <div class="col-sm-5">
                    <input type="text" class="form-control" name="Phone_num" value="{$edit.phone_num}" placeholder="Phone Number">
                </div>
            </div>
            <hr class="style12">
            <h3>Login Information</h3>

            <div class="form-group">
                <label for="Last_Name" class="control-label col-sm-4">User Name</label>
                <div class="col-sm-5">
                    <span style="color:red;">{if $error.user_name eq 1}Please input field user_name!{/if}</span>
                    <input type="text" class="form-control" name="user_name" value="{$edit.user_name}" placeholder="User Name">
                </div>
            </div>

            <div class="form-group">
                <label for="e_mail" class="control-label col-sm-4">E-mail</label>
                <div class="col-sm-5">
                    <input type="text" class="form-control" name="E_mail" value="{$edit.email}" placeholder="E-mail">
                </div>
            </div>

            <div class="form-group">
                <label for="password" class="control-label col-sm-4">Password</label>
                <div class="col-sm-5">
                    <input type="text" class="form-control" name="password" value="{$edit.password}" placeholder="Password">
                </div>
            </div>

            <div class="form-group">
                <label for="gender" class="control-label col-sm-4">Staff's Category</label>
                <div class="col-sm-5">
                  <select class="form-control" name="staff_category"  id="exampleFormControlSelect1">
                    <option value="1" {if $edit.staff_category eq 1}selected{/if}>Editor</option>
                    <option value="2" {if $edit.staff_category eq 2}selected{/if}>Author</option>
                    <option value="3" {if $edit.staff_category eq 3}selected{/if}>Other</option>
                  </select>
                </div>
            </div>

            <div class="form-group">
                <div class="col-sm-4" style="float:right">
                    {if $edit.id}
                         <button name="submit"  id="submit" type="sutmit" class="btn btn-success">update</button>
                    {else}
                        <button name="submit"  id="submit" type="sutmit" class="btn btn-success">save</button>
                        <button type="reset" class="btn btn-danger">Reset</button>
                    {/if}
                    
                </div>
            </div>
        </form>
    </div>
    
</div>
  <!-- /.content-wrapper -->
{/block}
{block name="javascript"}
<script>
$(document).ready(function(){
  $("#submit").click(function(){
   var First_Name = $("#First_Name").val();
   var Last_Name  = $("#Last_Name").val();
    validation(First_Name, Last_Name, '', '', '');
  });
});

$(function() {
  $('input[name="birthday"]').daterangepicker({
    singleDatePicker: true,
    showDropdowns: true,
    minYear: 1901,
    maxYear: parseInt(moment().format('YYYY'),10)
  }, function(start, end, label) {
    var years = moment().diff(start, 'years');
    alert("You are " + years + " years old!");
  });
});
   
</script>
{/block}
