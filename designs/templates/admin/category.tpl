{extends file="admin/layout.tpl"}
{block name="main"}
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Data
        <small>preview of All Category</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="{$admin_file_name}"><i class="fa fa-home" aria-hidden="true"></i></i>Home</a></li>
        <li class="active"><i class="fa fa-cog" aria-hidden="true"></i>Category</li>
      </ol>


    <!-- Main content -->

        {if $edit.id}
            <form class="form-horizontal" action="{$admin_file_name}?task=category&amp;action=edit" method="post">
        {else}
            <form class="form-horizontal" action="{$admin_file_name}?task=category&amp;action=add" method="post">
        {/if}
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-11">
                        <div class="form-group">
                            <label for="Albums"><h4><b>Name:</b></h4><span style="color:red;">{if $error.name eq 1}Please input field name!{/if}</span></label>
                            <input type="text" name="name" value="{$edit.name}" class="form-control default-border" id="title">
                        </div>
                    </div>
                    <div class="col-md-1">
                        <div class="form-group">
                            <label for="action"></label><br>
                            {if $edit.id}
                              <input type="hidden" name="id" value="{$edit.id}">
                              <button type="submit" name="button" class="btn btn-warning btn-md btn-block" style=" margin-top: 5px;"> Update</button>
                              <button type="submit" name="button" class="btn btn-danger btn-md btn-block"><a href="{$admin_file_name}?task=category" style="text-decoration: none;color:white;"> Back</a></button>
                            {else}
                              <button type="submit" name="button" class="btn btn-warning btn-md btn-block" style="margin-top: 23px;"><i class="fa fa-floppy-o" aria-hidden="true"></i> Save</button>
                            {/if}

                        </div>
                    </div>
                </div>
            </div>
        </form>
      <!-- /.row -->
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title"><b>Category</b></h3>
              <div class="box-tools pull-right">
                <div class="has-feedback">
                  <form class="" action="{$admin_file_name}" method="get">
                    <input type="hidden" name="task" value="category">
                    <input type="text" name="kwd" class="form-control input-sm" placeholder="Search Category">
                    <span class="glyphicon glyphicon-search form-control-feedback"></span>
                    <button type="submit">Search</button>
                  </form>
                </div>
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body table-responsive no-padding">
              <table class="table table-hover">
                <tr>
                  <th><h4><b>No</b></h4></th>
                  <th><h4><b>Name</b></h4></th>
                  <th><h4><b>Action</b></h4></th>
                </tr>
                    {if COUNT($list_category)>0}
                        {foreach from = $list_category item = v key=k}
                        <tr>
                            <td style="font-size:22px;">{$k+1}</td>
                            <td><h4>{$v.name}<h4></td>
                            <td>
                                <a href="{$admin_file_name}?task=sub_category&amp;cate_id={$v.id}" class="btn btn-info btn-md">Add SubCateogry</a>
                                    <a class="btn btn-warning btn-md" href="{$admin_file_name}?task=category&amp;action=edit&amp;id={$v.id}" ><i class="fa fa-pencil-square-o"></i>Edit</a>
                                    <button type="button" class="btn btn-danger btn-md" data-toggle="modal" data-target="#myModal_{$v.id}"><i class="fa fa-trash" aria-hidden="true"></i>Delete</button>
                                    <!-- Modal delete -->
                                    <div class="modal fade" id="myModal_{$v.id}" role="dialog">
                                        <div class="modal-dialog modal-sm">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                    <h4 class="modal-title">Confirmation</h4>
                                                </div>
                                                <div class="modal-body">
                                                    <p>Are you sure to delete?</p>
                                                </div>
                                                <div class="modal-footer">
                                                    <a class="btn btn-warning btn-md" href="{$admin_file_name}?task=category&amp;action=del&amp;id={$v.id}"><i class="fa fa-trash-o"></i>Delete</a>
                                                    <button type="button" class="btn btn-danger btn-md" data-dismiss="modal"><i class="fa fa-trash" aria-hidden="true"></i> Close</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                             </td>
                        </tr>
                        {/foreach}
                    {else}
                        <tr>
                            <td colspan="5" align ="center" style="font-size:16px;"> <i class="fa fa-exclamation-triangle"></i>No category</td>
                        </tr>
                    {/if}
                </table>
                {include file="common/paginate.tpl"}
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
      </div>
    </section>
    <!-- /.content -->
  </div>
  
{/block}
    {block name="javascript"}
{/block}
