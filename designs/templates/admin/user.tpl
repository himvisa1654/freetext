{extends file="admin/layout.tpl"}
{block name="main"}
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        User Account
        <small>preview of All User Account</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="{$admin_file_name}"><i class="fa fa-home" aria-hidden="true"></i></i>Home</a></li>
        <li><a href="#"><i class="fa fa-user" aria-hidden="true"></i>User</a></li>
        <li class="active"><i class="fa fa-cog" aria-hidden="true"></i>User Account</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-md-6">

        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
            <h3 class="box-title"><b>All User Account</b></h3>
              <div class="box-tools pull-right">
                <div class="has-feedback">
                  <input type="text" class="form-control input-sm" placeholder="Search Category">
                  <span class="glyphicon glyphicon-search form-control-feedback"></span>
                </div>
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body table-responsive no-padding">
              <table class="table table-hover">
               <th>N<sup>0</sup> </th>
                    <th>First_Name</th>
                    <th>Last_Name</th>
                    <th>E_mail</th>
                    <th>Re_Email</th>
                    <th>Gender</th>
                    <th>Date of Birth</th>
                    <th>Address</th>
                    <th>phone_num</th>
                    <th>Action</th>
                  </tr>
                  
                     {if COUNT($list_user_register) > 0}
                          {foreach from = $list_user_register item = v key=k}
                          <tr>
                            <td style="font-size:22px;">{$k+1}</td>
                            <td><h4>{$v.first_name}</h4></td>
                            <td><h4>{$v.last_name}</h4></td>
                            <td><h4>{$v.email}</h4></td>
                            <td><h4>{$v.re_email}</h4></td>
                            <td><h4>{if $v.gender eq 1}Male{elseif $v.gender eq 2} Female {else} Other {/if}</h4></td>
                            <td><h4>{$v.birthday}</h4></td>
                            <td><h4>{$v.address}</h4></td>
                            <td><h4>{$v.phone_num}</h4></td>
                            <td>
                                  <a class="btn btn-warning btn-md" href="{$admin_file_name}?task=user&amp;action=edit&amp;id={$v.id}" ><i class="fa fa-pencil-square-o"></i>EDIT</a>
                                  <button type="button" class="btn btn-danger btn-md" data-toggle="modal" data-target="#myModal_{$v.id}"><i class="fa fa-trash" aria-hidden="true"></i>DELETE</button>
                                  <!-- Modal delete -->
                                  <div class="modal fade" id="myModal_{$v.id}" role="dialog">
                                      <div class="modal-dialog modal-sm">
                                          <div class="modal-content">
                                              <div class="modal-header">
                                                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                  <h4 class="modal-title">Confirmation</h4>
                                              </div>
                                              <div class="modal-body">
                                                  <p>Are you sure to delete?</p>
                                              </div>
                                              <div class="modal-footer">
                                                  <a class="btn btn-warning btn-md" href="{$admin_file_name}?task=user&amp;action=del&amp;id={$v.id}"><i class="fa fa-trash-o"></i>DELETE</a>
                                                  <button type="button" class="btn btn-danger btn-md" data-dismiss="modal"><i class="fa fa-trash" aria-hidden="true"></i> Close</button>
                                              </div>
                                          </div>
                                      </div>
                                  </div>
                                  <button type="button" class="btn btn-primary btn-md" data-toggle="modal" data-target="#largeShoes"><i class="fa fa-pencil-square-o"></i>More</button>
                                  <div class="modal fade" id="largeShoes" tabindex="-1" role="dialog" aria-labelledby="modalLabelLarge" aria-hidden="true">
                                      <div class="modal-dialog modal-lg">
                                          <div class="modal-content">

                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                    </button>
                                                    <h4 class="modal-title" id="modalLabelLarge">Modal Title</h4>
                                                </div>

                                                <div class="modal-body">
                                                    Modal content...
                                                </div>

                                            </div>
                                          </div>
                                      </div>
                            </td>
                        </tr>
                  {/foreach}
                  {else}
                        <tr>
                            <td colspan="12" align ="center" style="font-size:16px;"> <i class="fa fa-exclamation-triangle"></i>No category</td>
                        </tr>
                  {/if}
              </table>
            </div>
            <!-- /.box-body -->
            

          </div>
          {include file="common/paginate.tpl"}
          <!-- /.box -->
        </div>
      </div>
    </section>
    <!-- /.content -->
  </div>
    {/block}
        {block name="javascript"}
    {/block}
