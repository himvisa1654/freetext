{extends file="index/layout.tpl"}
{include file="internal_libs/validate.class" assign=name var1=value}

{block name="main"}
  <!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">

  <section class="content-header">
      <h1>
        Register
        <small>preview of register for user</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="{$index_file_name}"><i class="fa fa-home" aria-hidden="true"></i></i>Home</a></li>
        <li><a href="{$index_file_name}?task=login"><i class="fa fa-user" aria-hidden="true"></i>Login</a></li>
        <li class="active">Register</li>
      </ol>
    </section>
    <div class="container-fluid" style="background-color:#ECF0F1;">
        <form action="{$index_file_name}?task=user&amp;action=add" class="form-horizontal" method="POST" style="margin-top:20px">
            <div class="form-group">
                <label for="First_Name" class="control-label col-sm-4">First Name</label>
                <div class="col-sm-5">
                    <span style="color:red;">{if $error.First_Name eq 1}Please input field First_Name!{/if}</span>
                    <input type="text" class="form-control" id="First_Name" name="First_Name" value="{$edit.First_Name}" placeholder="First Name">
                </div>
            </div>
        
            <div class="form-group">
                <label for="Last_Name" class="control-label col-sm-4">Last Name</label>
                <div class="col-sm-5">
                    <span style="color:red;">{if $error.Last_Name eq 1}Please input field Last_Name!{/if}</span>
                    <input type="text" class="form-control" name="Last_Name" value="{$edit.Last_Name}" placeholder="Last Name">
                </div>
            </div>

            <div class="form-group">
                <label for="e_mail" class="control-label col-sm-4">Your E-mail</label>
                <div class="col-sm-5">
                    <input type="text" class="form-control" name="E_mail" value="{$edit.email}" placeholder="E-mail">
                </div>
            </div>  

            <div class="form-group">
                <label for="e_mail" class="control-label col-sm-4">Re-enter E-mail</label>
                <div class="col-sm-5">
                    <input type="text" class="form-control" name="re_email" value="{$edit.re_email}" placeholder="Re-Enter E-mail">
                </div>
            </div>  

            <div class="form-group">
                <label for="gender" class="control-label col-sm-4">Gender</label>
                <div class="col-sm-5">
                  <select class="form-control" name="gender"  id="exampleFormControlSelect1">
                    <option value="1" {if $edit.gender eq 1}selected{/if}>M</option>
                    <option value="2" {if $edit.gender eq 2}selected{/if}>F</option>
                    <option value="3" {if $edit.gender eq 3}selected{/if}>Other</option>
                  </select>
                </div>
            </div>

            <div class="form-group">
                <label for="Last_Name" class="control-label col-sm-4">Date of Birth</label>
                <div class="col-sm-5">
                    <span style="color:red;">{if $error.First_Name eq 1}Please input field First_Name!{/if}</span>
    		        <input type="text" style="box-sizing: border-box;width: 100%;height: 34px;" name="birthday" value=" 10/24/2019" />
                </div>
            </div>

            <div class="form-group">
                <label for="Address" class="control-label col-sm-4">Address</label>
                <div class="col-sm-5">
                    <input type="text" class="form-control" name="Address" value="{$edit.address}" placeholder="Address">
                </div>
            </div>

            <div class="form-group">
                <label for="Phone Number" class="control-label col-sm-4">Phone Number</label>
                <div class="col-sm-5">
                    <input type="text" class="form-control" name="Phone_num" value="{$edit.phone_num}" placeholder="Phone Number">
                </div>
            </div>

            <div class="form-group">
                <div class="col-sm-4" style="float:right">
                    <button name="submit"  id="submit" type="sutmit" class="btn btn-success">save</button>
                    <button type="reset" class="btn btn-danger">Reset</button>
                </div>
            </div>
            
        </form>
    </div>
</div>
  <!-- /.content-wrapper -->
{/block}
{block name="javascript"}
<script>
$(function() {
  $('input[name="birthday"]').daterangepicker({
    singleDatePicker: true,
    showDropdowns: true,
    minYear: 1901,
    maxYear: parseInt(moment().format('YYYY'),10)
  }, function(start, end, label) {
    var years = moment().diff(start, 'years');
    alert("You are " + years + " years old!");
  });
});
</script>
{/block}
