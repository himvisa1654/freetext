$(document).ready(function() {

  // initialize
  $('select').material_select();


  $("#myButton").click(function() {

    // clear contents
    var $selectDropdown =
      $("#dropdownid")
        .empty()
        .html(' ');

    // add new value
    var value = "some value";
    $selectDropdown.append(
      $("<option></option>")
        .attr("value",value)
        .text(value)
    );

    // trigger event
    $selectDropdown.trigger('contentChanged');
  });


  $('select').on('contentChanged', function() {
    // re-initialize (update)
    $(this).material_select();
  });

});
