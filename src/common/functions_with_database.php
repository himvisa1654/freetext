<?php
function list_user_register($kwd = '')
{
  global $debug,$connected,$total_data,$limit,$offset;
  $result = true;
    try
    {
      $condition = $where = '';
      if(!empty($kwd))
      {
          if(!empty($condition)) $condition .= 'AND';
          $condition = ' reg.First_Name LIKE :kwd OR reg.Last_Name LIKE :kwd OR reg.user_name LIKE :kwd ';
      }
      if(!empty($condition)) $where = 'WHERE'.$condition;
      $sql = "SELECT reg.*,(SELECT COUNT(*) FROM tb_user AS reg ".$where.") AS total FROM tb_user AS reg ".$where;
      $stmt = $connected->prepare($sql.'ORDER BY reg.id DESC LIMIT :offset,:limit');
      $stmt->bindValue(':offset',$offset,PDO::PARAM_INT);
      $stmt->bindValue(':limit',$limit,PDO::PARAM_INT);
      if (!empty($kwd)) 
      {
        $stmt->bindValue(':kwd', '%'.$kwd.'%', PDO::PARAM_STR);
      }
      $stmt->execute();
      $rows = $stmt->fetchAll();
      if(COUNT($rows) > 0) $total_data = $rows[0]['total'];
        return $rows;
      }
      catch (PDOException $e)
      {
        echo 'Errors : Can not SELECT data !' . $e->getMessage();
        exit;
      }
      return $result;
}
 ?>
