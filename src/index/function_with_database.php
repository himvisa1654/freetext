<?php
// ================================Add User=====================================
function addUser_register($first_name,$last_name,$email,$re_email,$gender,$birthday,$address,$phone_num)
{
    global $debug,$connected;
    $result = true;
    try
    {
        $sql = "INSERT INTO tb_user(first_name,last_name,email,re_email,gender,birthday,address,phone_num) VALUE(:First_Name,:Last_Name,:E_mail,:re_email,:gender,:birthday,:Address,:Phone_num)";
        // var_dump($sql);exit;
        $stmt = $connected->prepare($sql);
        $stmt->bindValue(':First_Name',(string)$first_name,PDO::PARAM_STR);
        $stmt->bindValue(':Last_Name',(string)$last_name,PDO::PARAM_STR);
        $stmt->bindValue(':E_mail',(string)$email,PDO::PARAM_STR);
        $stmt->bindValue(':re_email',(string)$re_email,PDO::PARAM_STR);
        $stmt->bindValue(':gender',(string)$gender,PDO::PARAM_STR);
        $stmt->bindValue(':birthday',(string)$birthday,PDO::PARAM_STR);
        $stmt->bindValue(':Address',(string)$address,PDO::PARAM_STR);
        $stmt->bindValue(':Phone_num',(string)$phone_num,PDO::PARAM_STR);
        $stmt->execute();
    }
    catch(Exception $e)
    {
        $result = false;
        if($debug)
        {
            echo 'Errors : Add User Register' . $e->getMessage();
            exit;
        }
    }
    return $result;
}

//Edit or update
function updateRegisteraccount($id,$first_name,$last_name,$email,$address,$phone_num)
{
  global $debug,$connected,$total_data,$limit,$offset;
  $result = true;
  try {
    $sql = 'UPDATE tb_register SET First_Name=:First_Name,Last_Name=:Last_Name,email=:email,address=:address,phone_num=:phone_num WHERE id=:id';
    $stmt = $connected->prepare($sql);
    $stmt->bindValue(':id',(int)$id,PDO::PARAM_INT);
    $stmt->bindValue(':First_Name',(string)$first_name,PDO::PARAM_STR);
    $stmt->bindValue(':Last_Name',(string)$last_name,PDO::PARAM_STR);
    $stmt->bindValue(':email',(string)$email,PDO::PARAM_STR);
    $stmt->bindValue(':re_email',(string)$re_email,PDO::PARAM_STR);
    $stmt->bindValue(':gender',(string)$gender,PDO::PARAM_STR);
    $stmt->bindValue(':birthday',(string)$birthday,PDO::PARAM_STR);
    $stmt->bindValue(':Address',(string)$address,PDO::PARAM_STR);
    $stmt->bindValue(':Phone_num',(string)$phone_num,PDO::PARAM_STR);
    $stmt->execute();
  }
  catch (PDOException $e)
  {
    $result = false;
    if ($debug) echo "Error: Update Register Account".$e->getMessage();
  }

}
function getRegisterbyID($id)
{
    global $debug,$connected,$total_data,$limit,$offset;
    $result = true;
    try
    {
        $sql = 'SELECT * FROM tb_register WHERE id = :id';
        $stmt = $connected->prepare($sql);
        $stmt->bindValue(':id',(int)$id,PDO::PARAM_INT);
        $stmt->execute();
        $rows = $stmt->fetch();
        return $rows;
    }
    catch (PDOException $e)
    {
      $result = false;
      if($debug) echo 'Error: getRegisterByID',$e->getmessage();
    }
}
function getPhotoByCategory($id){
    global $debug, $connected, $limit, $offset, $total_data;
    $result = true;
    try {
        $condition = $where = "";
        if(!empty($id)){
            $where .= " pc.category_id = :id ";
        }
        if(!empty($where)) $condition .= ' WHERE '.$where;
        $join ='INNER JOIN image AS img
                    ON pc.photo_id= img.id
                INNER JOIN category AS cat
                    ON pc.category_id = cat.id ';
        $sql = ' SELECT pc.*, img.title AS image_title, img.thumbnail AS image_file,
                (SELECT COUNT(*) FROM photo_category AS pc '.$join.$condition.') AS total
                 FROM photo_category AS pc '.$join.$condition.' ORDER BY pc.id DESC LIMIT :offset, :limit ';
        $query = $connected->prepare($sql);
        $query->bindValue(':id',(int)$id, PDO::PARAM_INT);
        $query->bindValue(':offset', $offset, PDO::PARAM_INT);
        $query->bindValue(':limit', $limit, PDO::PARAM_INT);
        $query->execute();
        $rows = $query->fetchAll();
        //echo $rows; exit;
        if(COUNT($rows) > 0) $total_data = $rows[0]['total'];
        return $rows;
    }
    catch (Exception $e) {
        $result = false;
        if($debug)  echo 'Errors: getPhotoByCategory'.$e->getMessage();
    }
    return $result;
}
function getAllImage($kwd){
    global $debug, $connected, $limit, $offset, $total_data;
    $result = true;
    try {
        $condition = $where = "";
        if(!empty($kwd)){
            $where .= " title LIKE :kwd ";
        }
        if(!empty($where)) $condition .= ' WHERE '.$where;

        $sql = ' SELECT *, (SELECT COUNT(*) FROM image) AS total FROM image '.$condition.' ORDER BY id DESC LIMIT :offset, :limit ';
        $query = $connected->prepare($sql);
        // echo $sql;exit;
        if(!empty($kwd)) $query->bindValue(':kwd', '%'.$kwd.'%', PDO::PARAM_STR);
        $query->bindValue(':offset', $offset, PDO::PARAM_INT);
        $query->bindValue(':limit', $limit, PDO::PARAM_INT);
        $query->execute();
        $rows = $query->fetchAll();

        if(COUNT($rows) > 0) $total_data = $rows[0]['total'];
        return $rows;
    }
    catch (Exception $e) {
        $result = false;
        if($debug)  echo 'Errors: getAllImage'.$e->getMessage();
    }
    return $result;
}

function getCategoryById($id)
{
    global $debug, $connected, $total_data, $limit, $offset;
    $result = true;
    try
    {
        $sql  = ' SELECT * FROM category WHERE id = :id';
        $stmt = $connected->prepare($sql);
        $stmt->bindValue(':id',(int)$id, PDO::PARAM_INT);
        $stmt->execute();
        $rows = $stmt->fetch();
        return $rows;
    }
    catch (PDOException $e)  {
        $result = false;
        if($debug)  echo 'Errors: getCategoryById '.$e->getMessage();
    }
    return $result;
}
function getAlLCategory()
{
    global $debug, $connected, $total_data, $limit, $offset;
    $result = true;
    try
    {
        $sql  = ' SELECT * FROM category ORDER BY id DESC';
        $stmt = $connected->prepare($sql);
        $stmt->execute();
        $rows = $stmt->fetchAll();
        return $rows;
    }
    catch (PDOException $e)  {
        $result = false;
        if($debug)  echo 'Errors: getAlLCategory '.$e->getMessage();
    }
    return $result;
}
?>
