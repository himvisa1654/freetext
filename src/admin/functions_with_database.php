<?php
//===========================================Register=======================================
// Insert to Register
function addAccountRegister($first_name,$last_name,$gender,$email,$password,$address,$phone_num,$user_name,$staff_category,$birthday)
{
    global $debug,$connected;
    $result = true;
    try
    {
      $sql = "INSERT INTO tb_register(First_Name,Last_Name,email,gender,address,phone_num,password,user_name,staff_category,birthday) VALUE(:First_Name,:Last_Name,:email,:gender,:address,:phone_num,:password,:user_name,:staff_category,:birthday)";
        $stmt = $connected->prepare($sql);
        $stmt->bindValue(':First_Name',(string)$first_name,PDO::PARAM_STR);
        $stmt->bindValue(':Last_Name',(string)$last_name,PDO::PARAM_STR);
        $stmt->bindValue(':email',(string)$email,PDO::PARAM_STR);
        $stmt->bindValue(':gender',(string)$gender,PDO::PARAM_STR);
        $stmt->bindValue(':address',(string)$address,PDO::PARAM_STR);
        $stmt->bindValue(':phone_num',(string)$phone_num,PDO::PARAM_STR);
        $stmt->bindValue(':password',(string)$password,PDO::PARAM_STR);
        $stmt->bindValue(':user_name',(string)$user_name,PDO::PARAM_STR);
        $stmt->bindValue(':staff_category',(string)$staff_category,PDO::PARAM_STR);
        $stmt->bindValue(':birthday',(string)$birthday,PDO::PARAM_STR);
        $stmt->execute();
    }
    catch(Exception $e)
    {
        $result = false;
        if($debug)
        {
            echo 'Errors : Add Account Register' . $e->getMessage();
            exit;
        }
    }
    return $result;
}
function list_register($kwd = '')
{
  global $debug,$connected,$total_data,$limit,$offset;
  $result = true;
    try
    {
      $condition = $where = '';
      if(!empty($kwd))
      {
          if(!empty($condition)) $condition .= 'AND';
          $condition = ' reg.First_Name LIKE :kwd OR reg.Last_Name LIKE :kwd OR reg.user_name LIKE :kwd ';
      }
      if(!empty($condition)) $where = 'WHERE'.$condition;
      $sql = "SELECT reg.*,(SELECT COUNT(*) FROM tb_register AS reg ".$where.") AS total FROM tb_register AS reg ".$where;
      $stmt = $connected->prepare($sql.'ORDER BY reg.id DESC LIMIT :offset,:limit');
      $stmt->bindValue(':offset',$offset,PDO::PARAM_INT);
      $stmt->bindValue(':limit',$limit,PDO::PARAM_INT);
      if (!empty($kwd)) 
      {
        $stmt->bindValue(':kwd', '%'.$kwd.'%', PDO::PARAM_STR);
      }
      $stmt->execute();
      $rows = $stmt->fetchAll();
      if(COUNT($rows) > 0) $total_data = $rows[0]['total'];
        return $rows;
      }
      catch (PDOException $e)
      {
        echo 'Errors : Can not SELECT data !' . $e->getMessage();
        exit;
      }
      return $result;

}

//Edit or update
function updateRegisteraccount($id,$first_name,$last_name,$gender,$email,$password,$address,$phone_num,$user_name,$staff_category,$birthday)
{
  global $debug,$connected,$total_data,$limit,$offset;
  $result = true;
  try {
    $sql = 'UPDATE tb_register SET First_Name=:First_Name,Last_Name=:Last_Name,email=:email,gender=:gender,address=:address,phone_num=:phone_num,password=:password WHERE id=:id';
    $stmt = $connected->prepare($sql);
    $stmt->bindValue(':id',(int)$id,PDO::PARAM_INT);
    $stmt->bindValue(':First_Name',(string)$first_name,PDO::PARAM_STR);
    $stmt->bindValue(':Last_Name',(string)$last_name,PDO::PARAM_STR);
    $stmt->bindValue(':email',(string)$email,PDO::PARAM_STR);
    $stmt->bindValue(':gender',(string)$gender,PDO::PARAM_STR);
    $stmt->bindValue(':address',(string)$address,PDO::PARAM_STR);
    $stmt->bindValue(':phone_num',(string)$phone_num,PDO::PARAM_STR);
    $stmt->bindValue(':password',(string)$password,PDO::PARAM_STR);
    $stmt->execute();
  }
  catch (PDOException $e)
  {
    $result = false;
    if ($debug) echo "Error: Update Register Account".$e->getMessage();
  }

}
function deleteRegisteraccount($id)
{
  global $debug,$connected;
  $result = true;
  try
  {
    $sql = 'DELETE FROM tb_register WHERE id = :id';
    $stmt = $connected->prepare($sql);
    $stmt->bindValue(':id',(int)$id, PDO::PARAM_INT);
    $stmt->execute();
  }
  catch (Exception $e)
  {
   $result = false;
    if ($debug)
    {
      echo 'ERROR:Delete deleteCategory' . $e->getMessage();
      exit;
    }
  }
  return $result;
}
// Get_Register_ByID
function getRegisterbyID($id)
{
    global $debug,$connected,$total_data,$limit,$offset;
    $result = true;
    try
    {
        $sql = 'SELECT * FROM tb_register WHERE id = :id';
        $stmt = $connected->prepare($sql);
        $stmt->bindValue(':id',(int)$id,PDO::PARAM_INT);
        $stmt->execute();
        $rows = $stmt->fetch();
        return $rows;
    }
    catch (PDOException $e)
    {
      $result = false;
      if($debug) echo 'Error: getRegisterByID',$e->getmessage();
    }
}
//=============================User_Register_Account===================================
function update_user_registeraccount($id,$first_name,$last_name,$email,$address,$phone_num)
{
  global $debug,$connected,$total_data,$limit,$offset;
  $result = true;
  try {
    $sql = 'UPDATE tb_user SET first_name=:first_name,last_name=:last_name,email=:email,address=:address,phone_num=:phone_num WHERE id=:id';
    $stmt = $connected->prepare($sql);
    $stmt->bindValue(':id',(int)$id,PDO::PARAM_INT);
    $stmt->bindValue(':first_name',(string)$first_name,PDO::PARAM_STR);
    $stmt->bindValue(':last_name',(string)$last_name,PDO::PARAM_STR);
    $stmt->bindValue(':email',(string)$email,PDO::PARAM_STR);
    $stmt->bindValue(':address',(string)$address,PDO::PARAM_STR);
    $stmt->bindValue(':phone_num',(string)$phone_num,PDO::PARAM_STR);
    $stmt->execute();
  }
  catch (PDOException $e)
  {
    $result = false;
    if ($debug) echo "Error: Update Register Account".$e->getMessage();
  }

}
//=======================================Category==========================================
function updateCategory($id,$name)
{
    global $debug, $connected, $total_data, $limit, $offset;
    $result = true;
    try
    {
        $sql  = ' UPDATE category SET name=:name WHERE  id = :id';
        $stmt = $connected->prepare($sql);//prepare it is create for send data to DB
        $stmt->bindValue(':id',(int)$id, PDO::PARAM_INT);
        $stmt->bindValue(':name',(string)$name, PDO::PARAM_STR);
        $stmt->execute();
    }
    catch (PDOException $e)
    {
        $result = false;
        if($debug)  echo 'Errors: updateCategory '.$e->getMessage();
    }
    return $result;
}

function getCategoryById($id)
{
    global $debug, $connected, $total_data, $limit, $offset;
    $result = true;
    try
    {
        $sql  = ' SELECT * FROM category WHERE id = :id';
        $stmt = $connected->prepare($sql);
        $stmt->bindValue(':id',(int)$id, PDO::PARAM_INT);
        $stmt->execute();
        $rows = $stmt->fetch();
        return $rows;
    }
    catch (PDOException $e)  {
        $result = false;
        if($debug)  echo 'Errors: getCategoryById '.$e->getMessage();
    }
    return $result;
}
//sub category
function addSubCategory($cate_id, $sub_name)
{
    global $debug,$connected;
    $result = true;
    try
    {
      $sql = 'INSERT INTO category(	parent_id, name) VALUES(:parent_id, :sub_name)';
      $stmt = $connected->prepare($sql);
      $stmt->bindValue(':parent_id',(int)$cate_id, PDO::PARAM_INT);
      $stmt->bindValue(':sub_name',(string)$sub_name, PDO::PARAM_STR);
      $stmt->execute();
    }
    catch (Exception $e)
    {
      $result = false;
      if ($debug)
      {
          echo 'ERROR: Add addSubCategory' . $e->getMessage();
          exit;
      }
    }
    return $result;
}
//list sub category
function listSubCategory($parent_id)
{
    global $debug, $connected, $total_data, $limit, $offset;
    $result = true;
    try
    {
        $sql  = 'SELECT * FROM category WHERE parent_id = :parent_id';
        $stmt = $connected->prepare($sql);
        $stmt->bindValue(':parent_id',(int)$parent_id, PDO::PARAM_INT);
        $stmt->execute();
        $rows = $stmt->fetchAll();
        return $rows;
    }
    catch (PDOException $e)  {
        $result = false;
        if($debug)  echo 'Errors: listSubCategory '.$e->getMessage();
    }
    return $result;
}
function getSubCategoryById($parent_id)
{
    global $debug, $connected, $total_data, $limit, $offset;
    $result = true;
    try
    {
        $sql  = 'SELECT * FROM category WHERE parent_id = :parent_id';
        $stmt = $connected->prepare($sql);
        $stmt->bindValue(':parent_id',(int)$parent_id, PDO::PARAM_INT);
        $stmt->execute();
        $rows = $stmt->fetch();
        return $rows;
    }
    catch (PDOException $e)
    {
        $result = false;
        if($debug)  echo 'Errors: listSubCategory '.$e->getMessage();
    }
    return $result;
}
function updateSubCategory($id,$name)
{
    global $debug, $connected, $total_data, $limit, $offset;
    $result = true;
    try
    {
        $sql  = ' UPDATE category SET name=:name WHERE  id = :id';
        $stmt = $connected->prepare($sql);
        $stmt->bindValue(':id',(int)$id, PDO::PARAM_INT);
        $stmt->bindValue(':name',(string)$name, PDO::PARAM_STR);
        $stmt->execute();
    }
    catch (PDOException $e)
    {
        $result = false;
        if($debug)  echo 'Errors: updateSubCategory '.$e->getMessage();
    }
    return $result;
}
function deleteSubCategory($id)
{
    global $debug,$connected;
    $result = true;
    try
    {
      $sql = 'DELETE FROM category WHERE id = :id';
      $stmt = $connected->prepare($sql);
      $stmt->bindValue(':id',(int)$id, PDO::PARAM_INT);
      $stmt->execute();
    }
    catch (Exception $e)
    {
     $result = false;
      if ($debug)
      {
        echo 'ERROR:Delete deleteSubCategory' . $e->getMessage();
        exit;
      }
    }
    return $result;
}
function addCategory($name)
{
    global $debug,$connected;
    $result = true;
    try
    {
      $sql = 'INSERT INTO category(name) VALUES(:name)';
      $stmt = $connected->prepare($sql);
      $stmt->bindValue(':name',(string)$name, PDO::PARAM_STR);
      $stmt->execute();
    }
    catch (PDOException $e)
    {
      $result = false;
      if ($debug)
      {
        echo 'ERROR: Add addCategory' . $e->getMessage();
        exit;
      }
    }
    return $result;
}
/**
* List Category by id
* @author Lyly
* @param string $name
*/
function listCateogry($kwd = '', $sub) {
    global $debug, $connected, $total_data, $limit, $offset;
    $result = true;
    try
    {
        $condition = $where = '';
        if(!empty($kwd))
        {
            if(!empty($condition)) $condition .= ' AND ';
            $condition = ' cat.name LIKE :kwd ';
        }
        if(!empty($sub))
        {
            if(!empty($condition)) $condition .= ' AND ';
            $condition = ' cat.parent_id = 0';
        }
        if(!empty($condition)) $where = ' WHERE '.$condition;
        $sql  = ' SELECT cat.*,(SELECT COUNT(*) FROM category AS cat '.$where.') AS total
        FROM category AS cat '.$where;
        $stmt = $connected->prepare($sql.' ORDER BY cat.id DESC LIMIT :offset, :limit');
        $stmt ->bindValue(':offset', $offset, PDO::PARAM_INT);
        $stmt ->bindValue(':limit', $limit, PDO::PARAM_INT);
        if(!empty($kwd)) $stmt->bindValue(':kwd', '%'.$kwd.'%', PDO::PARAM_STR);
        $stmt->execute();
        $rows = $stmt->fetchAll();
        if(count($rows) > 0) $total_data = $rows[0]['total'];
        return $rows;
    }
    catch (PDOException $e)
    {
        $result = false;
        if($debug)  echo 'Errors: listCateogry '.$e->getMessage();
    }
    return $result;
}
function deleteCategory($id)
{
    global $debug,$connected;
    $result = true;
    try
    {
      $sql = 'DELETE FROM category WHERE id = :id';
      $stmt = $connected->prepare($sql);
      $stmt->bindValue(':id',(int)$id, PDO::PARAM_INT);
      $stmt->execute();
    }
    catch (Exception $e)
    {
     $result = false;
      if ($debug)
      {
        echo 'ERROR:Delete deleteCategory' . $e->getMessage();
        exit;
      }
    }
    return $result;
}
// Account
function getAccountById($id)
{
    global $debug, $connected, $total_data, $limit, $offset;
    $result = true;
    try
    {
        $sql  = ' SELECT * FROM account WHERE id = :id';
        $stmt = $connected->prepare($sql);
        $stmt->bindValue(':id',(int)$id, PDO::PARAM_INT);
        $stmt->execute();
        $rows = $stmt->fetch(); // bos data back
        return $rows;
    }
    catch (PDOException $e)
    {
        $result = false;
        if($debug)  echo 'Errors: getAccountById '.$e->getMessage();
    }
    return $result;
}

function addAccount($name, $email, $password, $start_date)
{
    global $debug,$connected;
    $result = true;
    try
    {
      $sql = 'INSERT INTO account(name, email, password,start_date) VALUES(:name, :email, :password, :start_date)';
      $stmt = $connected->prepare($sql);
      $stmt->bindValue(':name',(string)$name, PDO::PARAM_STR);
      $stmt->bindValue(':email',(string)$email, PDO::PARAM_STR);
      $stmt->bindValue(':password',(string)$password, PDO::PARAM_STR);
      $stmt->bindValue(':start_date',(string)$start_date, PDO::PARAM_STR);
      $stmt->execute();
    }
    catch (Exception $e)
    {
      $result = false;
      if ($debug)
      {
        echo 'ERROR: Add addAccount' . $e->getMessage();
        exit;
      }
    }
    return $result;
}
/**
   * updateAccount
   * @param [int] $id
   * @param [string] $name
   * @param [string] $email
   * @param [string] $password
   * @author: LYLY
   * @return boolean
 **/
function updateAccount($id, $name, $email, $password)
{
    global $debug, $connected, $total_data, $limit, $offset;
    $result = true;
    try
    {
        $sql  = ' UPDATE account SET name=:name, email=:email, password=:password WHERE  id = :id';
        $stmt = $connected->prepare($sql);
        $stmt->bindValue(':id',(int)$id, PDO::PARAM_INT);
        $stmt->bindValue(':name',(string)$name, PDO::PARAM_STR);
        $stmt->bindValue(':email',(string)$email, PDO::PARAM_STR);
        $stmt->bindValue(':password',(string)$password, PDO::PARAM_STR);
        $stmt->execute();
    }
    catch (PDOException $e)
    {
        $result = false;
        if($debug)  echo 'Errors: getAccountById '.$e->getMessage();
    }
    return $result;
}
//delete data
function deleteAccount($id)
{
    global $debug,$connected;
    $result = true;
    try
    {
      $sql = 'DELETE FROM account WHERE id = :id';
      $stmt = $connected->prepare($sql);
      $stmt->bindValue(':id',(int)$id, PDO::PARAM_INT);
      $stmt->execute();
    }
    catch (Exception $e)
    {
     $result = false;
      if ($debug)
      {
        echo 'ERROR:Delete deleteAccount' . $e->getMessage();
        exit;
      }
    }
    return $result;
}
//list account
function listAccount($kwd = '') {
    global $debug, $connected, $total_data, $limit, $offset;
    $result = true;
    try
    {
        $condition = $where = '';
        if(!empty($kwd))
        {
            if(!empty($condition)) $condition .= ' AND ';
            $condition = ' acc.name LIKE :kwd ';
        }
        if(!empty($condition)) $where = ' WHERE '.$condition;
        $sql  = ' SELECT acc.*,(SELECT COUNT(*) FROM account AS acc '.$where.') AS total
        FROM account AS acc '.$where;
        $stmt = $connected->prepare($sql.' ORDER BY acc.id DESC LIMIT :offset, :limit');
        $stmt ->bindValue(':offset', $offset, PDO::PARAM_INT);
        $stmt ->bindValue(':limit', $limit, PDO::PARAM_INT);
        if(!empty($kwd)) $stmt->bindValue(':kwd', '%'.$kwd.'%', PDO::PARAM_STR);
        $stmt->execute();
        $rows = $stmt->fetchAll();
        if(count($rows) > 0) $total_data = $rows[0]['total'];
        return $rows;
    }
    catch (PDOException $e)
    {
        $result = false;
        if($debug)  echo 'Errors: listAccount '.$e->getMessage();
    }
    return $result;
}
/**
   * [getAllAccount]
   * @author: LYLY
   * @return boolean
 **/
function getAllAccount()
{
    global $debug, $connected, $total_data, $limit, $offset;
    $result = true;
    try
    {
        $sql  = ' SELECT * FROM account ORDER BY id DESC';
        $stmt = $connected->prepare($sql);
        $stmt->execute();
        $rows = $stmt->fetchAll();
        return $rows;
    }
    catch (PDOException $e)
    {
        $result = false;
        if($debug)  echo 'Errors: getAllAccount '.$e->getMessage();
    }
    return $result;
}
//PAyment
function getPaymentById($id)
{
    global $debug, $connected, $total_data, $limit, $offset;
    $result = true;
    try
    {
        $sql  = ' SELECT * FROM payment WHERE id = :id';
        $stmt = $connected->prepare($sql);
        $stmt->bindValue(':id',(int)$id, PDO::PARAM_INT);
        $stmt->execute();
        $rows = $stmt->fetch(); // bos data back
        return $rows;
    }
    catch (PDOException $e)
    {
        $result = false;
        if($debug)  echo 'Errors: getPaymentById '.$e->getMessage();
    }
    return $result;
}
// add payment
function addPayment($acc_id, $fee, $start_date, $end_date)
{
    global $debug,$connected;
    $result = true;
    try
    {
      $sql = 'INSERT INTO payment(account_id, fee, start_date, end_date) VALUES(:acc_id, :fee, :start_date, :end_date)';
      $stmt = $connected->prepare($sql);
      $stmt->bindValue(':acc_id',(int)$acc_id, PDO::PARAM_INT);
      $stmt->bindValue(':fee',(int)$fee, PDO::PARAM_INT);
      $stmt->bindValue(':start_date',(string)$start_date, PDO::PARAM_STR);
      $stmt->bindValue(':end_date',(string)$end_date, PDO::PARAM_STR);
      $stmt->execute();
    }
    catch (Exception $e)
    {
      $result = false;
      if ($debug)
      {
        echo 'ERROR: Add addPayment' . $e->getMessage();
        exit;
      }
    }
    return $result;
}

function listPayment($kwd = '') {
    global $debug, $connected, $total_data, $limit, $offset;
    $result = true;
    try
    {
        $condition = $where = '';
        if(!empty($kwd))
        {
            if(!empty($condition)) $condition .= ' AND ';
            $condition = ' acc.name LIKE :kwd ';
        }
        if(!empty($condition)) $where = ' WHERE '.$condition;
        $sql  = 'SELECT pay.*, acc.name AS accname,(SELECT COUNT(*) FROM payment AS pay '.$where.') AS total FROM payment AS pay
        INNER JOIN account AS acc
            ON pay.account_id = acc.id
        '.$where.' ORDER BY pay.id DESC LIMIT :offset, :limit';
        $stmt = $connected->prepare($sql);
        $stmt ->bindValue(':offset', $offset, PDO::PARAM_INT);
        $stmt ->bindValue(':limit', $limit, PDO::PARAM_INT);
        if(!empty($kwd)) $stmt->bindValue(':kwd', '%'.$kwd.'%', PDO::PARAM_STR);
        $stmt->execute();
        $rows = $stmt->fetchAll();
        if(count($rows) > 0) $total_data = $rows[0]['total'];
        return $rows;
    }
    catch (PDOException $e)
    {
        $result = false;
        if($debug)  echo 'Errors: listPayment '.$e->getMessage();
    }
    return $result;
}
function updatePayment($id, $fee, $start_date, $end_date)
{
    global $debug, $connected, $total_data, $limit, $offset;
    $result = true;
    try
    {
        $sql  = ' UPDATE payment SET  fee=:fee, start_date=:start_date, end_date=:end_date WHERE  id = :id';
        $stmt = $connected->prepare($sql);
        $stmt->bindValue(':id',(int)$id, PDO::PARAM_INT);
        // $stmt->bindValue(':start_date',(string)$start_date, PDO::PARAM_STR);
        $stmt->bindValue(':fee',(int)$fee, PDO::PARAM_INT);
        $stmt->bindValue(':start_date',(string)$start_date, PDO::PARAM_STR);
        $stmt->bindValue(':end_date',(string)$end_date, PDO::PARAM_STR);
        $stmt->execute();
    }
    catch (PDOException $e)  {
        $result = false;
        if($debug)  echo 'Errors: updatePaymentById '.$e->getMessage();
    }
    return $result;
}
function deletePayment($id)
{
    global $debug,$connected;
    $result = true;
    try
    {
      $sql = 'DELETE FROM payment WHERE id = :id';
      $stmt = $connected->prepare($sql);
      $stmt->bindValue(':id',(int)$id, PDO::PARAM_INT);
      $stmt->execute();
    } catch (Exception $e) {
     $result = false;
      if ($debug)
      {
        echo 'ERROR:Delete deletePayment' . $e->getMessage();
        exit;
      }
    }
    return $result;
}
 ?>
