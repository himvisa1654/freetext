<?php
// ini_set('error_reporting', E_ALL);
// ini_set('display_errors', true);
error_reporting(0);
session_start();
//Load setting file
require_once('setup.php');
//Load database class
require_once(dirname(__FILE__).'/internal_libs/database.class.php');
require_once(dirname(__FILE__).'/internal_libs/common.class.php');
require_once(dirname(__FILE__).'/external_libs/thumbnail.php');
require_once(dirname(__FILE__).'/external_libs/Smarty-3.1.14/libs/SmartyPaginate.class.php');
require_once(dirname(__FILE__).'/src/admin/functions_with_database.php');
require_once(dirname(__FILE__).'/src/common/functions_with_database.php');

$common = new common();
$task = $action = $lang = $kwd = null;
if(!empty($_GET['task'])) 		$task   = $_GET['task'];
if(!empty($_GET['action']))		$action = $_GET['action'];
if(!empty($_GET['kwd'])) 		$kwd    = $_GET['kwd'];

$smarty_appform = new PHOTO_SMARTY();
$smarty_appform->assign('smpaginate','common/paginate.tpl');
$smarty_appform->assign('mode', 'admin');
$smarty_appform->assign('admin_file_name', $admin_file_name);
$smarty_appform->assign('index_file_name', $index_file_name);

/* Paginate */
if(empty($_SESSION['task'])) 	$_SESSION['task'] = $task;
if(empty($_SESSION['action'])) 	$_SESSION['action'] = $action;
if(empty($_SESSION['kwd']))		$_SESSION['kwd'] = $kwd;

if ($_SESSION['task'] != $task) SmartyPaginate::disconnect();
if (($_SESSION['task'] == $task) and ($_SESSION['action'] != $action)) SmartyPaginate::disconnect();
if (($_SESSION['task'] == $task) and ($_SESSION['action'] == $action) and $_SESSION['kwd'] != $kwd) SmartyPaginate::disconnect();

SmartyPaginate::disconnect();
SmartyPaginate::connect();
SmartyPaginate::setLimit($limit);
if(SmartyPaginate::getCurrentIndex())			$offset = SmartyPaginate::getCurrentIndex();
if(SmartyPaginate::getLimit())					$limit = SmartyPaginate::getLimit();

$geturl = '?task='.$task.'&action='.$action.'&kwd='.urlencode($kwd);
if(!empty($_GET['id']))    $geturl .= "&id=".$_GET['id'];
SmartyPaginate::setUrl($geturl);
/* End Paginate */
$total_data = null;
$error = '';

// Database connection
$database_connect 		 	= new database(DB_HOSTNAME, DB_USER,DB_PASSWORD, DB_DATABASE_NAME);
$database_connect->debug 	= $debug;
$connected 				 	= & $database_connect->_Connect;

//Task login
if('login' === $task)
{
	$_SESSION['is_login'] = '';
	$error = 0;
	if($_POST)
	{
		if($_POST['username'] === $username && md5($_POST['password'])=== $password)
		{
			$_SESSION['is_login'] = 'yes';

			header('location:'.$admin_file_name);
			exit;
		}else
		{
			$error = 1;
		}
	}
	$smarty_appform->assign('error', $error);
	$smarty_appform->display('admin/login.tpl');
	exit;
}
//Task logout
if('logout' === $task)
{
	$_SESSION['is_login'] = '';
	unset($_SESSION['is_login']);//The unset() function destroys a given variable.
	header('location:'.$admin_file_name.'?task=login');
	exit;
}
//Admin_Account
// list_register Account
if('admin_account' === $task)
{
	$error = array();
	$_SESSION['Staff'] = '';
	if('add' === $action)
	{
		if($_POST)
		{
			$first_name = $common->clean_string($_POST['First_Name']);
			$last_name = $common->clean_string($_POST['Last_Name']);
			$gander = $common->clean_string($_POST['gender']);
			$email = $common->clean_string($_POST['E_mail']);
			$Password = $common->clean_string($_POST['password']);
			$address = $common->clean_string($_POST['Address']);
			$phone_num = $common->clean_string($_POST['Phone_num']);
			$user_name = $common->clean_string($_POST['User_name']);
			$staff_category = $common->clean_string($_POST['staff_category']);
			$birthday = $common->clean_string($_POST['birthday']);

			if(empty($first_name))$error['First_Name'] = 1;

			if(empty($last_name))$error['Last_Name'] = 1;

			if (empty($gander))$gander['gender'] = 1;

			if(empty($email))$error['E_mail'] = 1;

			if(empty($Password))$error['password'] = 1;

			if(empty($address))$error['Address'] = 1;

			if(empty($phone_num))$error['Phone_num'] = 1;

			if(empty($user_name))$error['User_name'] = 1;

			if(empty($staff_category))$error['staff_category'] = 1;

			if(empty($birthday))$error['birthday'] = 1;

			if(count($error) == 0)
			{
				// create function insert
				addAccountRegister($first_name,$last_name,$gander,$email,$Password,$address,$phone_num,$user_name,$staff_category,$birthday);
				unset($_SESSION['Staff']);
				header('location:'.$admin_file_name.'?task=admin_account');
				var_dump(addAccountRegister);exit;
				exit;
			}
		}

		$smarty_appform->display('admin/register.tpl');
		exit;
	}
	//action edit
	if('edit' === $action)
	{
		if($_POST)
		{
			$id = $common->clean_string($_POST['id']);
			$first_name = $common->clean_string($_POST['First_Name']);
			$last_name = $common->clean_string($_POST['Last_Name']);
			$gander = $common->clean_string($_POST['gender']);
			$email = $common->clean_string($_POST['E_mail']);
			$password = $common->clean_string($_POST['password']);
			$address = $common->clean_string($_POST['Address']);
			$phone_num = $common->clean_string($_POST['Phone_num']);
			$user_name = $common->clean_string($_POST['User_name']);
			$staff_category = $common->clean_string($_POST['staff_category']);
			$birthday = $common->clean_string($_POST['birthday']);
			if(count($error) == 0)
			{
				updateRegisteraccount($id,$first_name,$last_name,$gander,$email,$password,$address,$phone_num,$user_name,$birthday);
				var_dump($first_name);exit;
				header('location:'.$admin_file_name.'?task=admin_account');
			}
		}
		if(!empty($_GET['id']))
		{
			$result_edit = getRegisterbyID($_GET['id']);
			// var_dump($result_edit);exit;
			$smarty_appform->assign('edit', $result_edit);
			// var_dump($result_edit);exit;
		}
		$smarty_appform->display('admin/register.tpl');
		exit;
	}
	//action delete
	if('del' === $action)
	{
		if(!empty($_GET['id']))
		{
			$id = $_GET['id'];
			deleteRegisteraccount($id);
			header('location'.$admin_file_name.'?task=admin_account.tpl');
		}
	}
	$kwd = '';
	if(!empty($_GET['kwd'])) $kwd = $_GET['kwd'];
	$listRegister = list_register($kwd);
	// var_dump($listRegister);exit;
	if($total_data > 0)	
	{
		SmartyPaginate::setTotal($total_data);
	}
	else
	{
		SmartyPaginate::setTotal(1);
	}
	SmartyPaginate::assign($smarty_appform);
	$smarty_appform->assign('listRegister',$listRegister);
	$smarty_appform->assign('error', $error);
	$smarty_appform->display('admin/admin_account.tpl');
	exit;
}
// Task Payment
if('payment' === $task)
{
	$smarty_appform->display('admin/payment.tpl');
	exit;
}
//task user_account
if('user' === $task)
{
	if('edit' === $action)
	{
		if($_POST)
		{
			$first_name = $common->clean_string($_POST['First_Name']);
			$last_name = $common->clean_string($_POST['Last_Name']);
			$email = $common->clean_string($_POST['E_mail']);
			$re_email = $common->clean_string($_POST['re_email']);
			$address = $common->clean_string($_POST['Address']);
			$phone_num = $common->clean_string($_POST['Phone_num']);
			if(count($error) == 0)
			{
				update_user_registeraccount($id,$first_name,$last_name,$email,$address,$phone_num);
				// var_dump($first_name);exit;
				header('location:'.$admin_file_name.'?task=user');
			}
		}
		if(!empty($_GET['id']))
		{
			$result_edit = getRegisterbyID($_GET['id']);
			// var_dump($result_edit);exit;
			$smarty_appform->assign('edit', $result_edit);
			// var_dump($result_edit);exit;
		}
		$smarty_appform->display('common/register.tpl');
		exit;
	}
	$kwd = '';
	if(!empty($_GET['kwd'])) $kwd = $_GET['kwd'];
	$list_user_register = list_user_register($kwd);
	// var_dump($listRegister);exit;
	if($total_data > 0)	
	{
		SmartyPaginate::setTotal($total_data);
	}
	else
	{
		SmartyPaginate::setTotal(1);
	}
	$smarty_appform->assign('list_user_register',$list_user_register);
	$smarty_appform->display('admin/user.tpl');
	exit;
}
//task view_all_user
if('view_all_user' === $task)
{
	$smarty_appform->display('admin/view_all_user.tpl');
	exit;
}
if(empty($_SESSION['is_login']))
{
	header('location:'.$admin_file_name.'?task=login');
	exit;
}

// task category
if('category'===$task)
{
	$error = array();
	if('add' === $action)
	{
		if($_POST)
		{
			$name = $common->clean_string($_POST['name']);
			if(empty($name))   $error['name'] = 1;

			if(count($error) == 0)
			{
				// create function insert
				addCategory($name);
				header('location:'.$admin_file_name.'?task=category');
				exit;
			}
		}
	}

	if('del' === $action) //=== compare string
	{
		if(!empty($_GET['id']))
		{
			$id=$_GET['id'];
			deleteCategory($id);
			header('location:'.$admin_file_name.'?task=category');
			exit;
		}
	}
	if('edit' === $action)
	{
		if($_POST)
		{
			$name = $common->clean_string($_POST['name']); // security
			$id = $common->clean_string($_POST['id']);
			if(empty($name))   $error['name'] = 1;
			if(count($error) == 0)
			{
				updateCategory($id,$name);
				header('location:'.$admin_file_name.'?task=category');
				exit;
			}
		}
		if(!empty($_GET['id']))
		{
			$result_edit = getCategoryById($_GET['id']);
			$smarty_appform->assign('edit', $result_edit);
		}
	}

	$kwd='';
	if(!empty($_GET['kwd'])) $kwd = $_GET['kwd'];
	$listcategory = listCateogry($kwd, $sub=1);
	if($total_data > 0)
	{
		SmartyPaginate::setTotal($total_data);
	}
	else
	{
		SmartyPaginate::setTotal(1);
	}
	SmartyPaginate::assign($smarty_appform);
	$smarty_appform->assign('list_category', $listcategory);
	$smarty_appform->assign('delete_category', $deletecategory);
	$smarty_appform->assign('error', $error);
	$smarty_appform->display('admin/category.tpl');
	exit;
}
//Parsing template
$smarty_appform->display('admin/index.tpl');
exit;
?>
