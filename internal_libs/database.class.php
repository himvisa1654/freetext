<?php

/**
 * File: database.class.php
 *
 * Database connection libary
 *
 * @copyright 2013 E-KHMER.
 * @author Sengtha Chay <sengtha@gmail.com>
 * @version 0.1
 */
class database
{
	public $debug = false;
	public $_Connect = null;
	/**
	* Open database connection
	* @param string $hostname Hostname
	* @param string $user Username
	* @param string $password Password
	* @param string $dbname Database Name
	*/
	public function __construct($hostname, $user, $password, $dbname)
	{

		try
		{
      $this->_Connect = new PDO("mysql:host=$hostname;dbname=$dbname", $user, $password);
			$this->_Connect->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		}
		catch(Exception $e)
		{
			if ($this->debug) echo "Error：". $e->getMessage();

		}

  }

	/*
	*
	* Close database connection
	*/
   	public function __destruct()
		 {
    		$this->_Connect = null;
   	}
}

?>
