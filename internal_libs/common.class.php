<?php
/**
 * File: common.class.php
 *
 * Common library
 * @copyright 2009 E-KHMER.
 * @author Sengtha Chay <sengtha@gmail.com>
 * @version 0.1
 */
class common
{
	function common(){}
	//Get remote content
	function clean_string($str)
	{
		$str = strip_tags($str);
		$str = stripslashes($str);
		return $str;
	}

	//Check password
  function checkPassword($pwd) {
    if (strlen($pwd) < 8) {
        //'Password too short!'
        return false;
    }
    if (!preg_match("#[0-9]+#", $pwd)) {
        //'Password must include at least one number!';
        return false;
    }
    if (!preg_match("#[a-zA-Z]+#", $pwd)) {
        //'Password must include at least one letter!';
        return false;
    }
    return true;
}
	//Upload file
	function upload_real_file($file, $toname, $path, $formname, $allows)
	{
		$uploadfile = $path . $toname;
		$path_info = pathinfo($uploadfile);
		if(!in_array($path_info['extension'],$allows["EXT"]))
		    return 1;
		if($file[$formname]['size']>$allows["SIZE"][0])
			return 2;
		if (move_uploaded_file($file[$formname]['tmp_name'], $uploadfile))
		{
			return $toname;
		}
		return 0;
	}
    //Upload file

	function upload_file($file, $id, $path, $formname, $allows)
	{

		$uploadfile = $path . $id."__".basename($file[$formname]['name']);
		$path_info = pathinfo($uploadfile);
		if(!in_array($path_info['extension'],$allows["EXT"]))
		      return 1;
		if($file[$formname]['size']>$allows["SIZE"][0])
			return 2;
		if (move_uploaded_file($file[$formname]['tmp_name'], $uploadfile))
		{
			return $id."__".basename($file[$formname]['name']);
		}
		return 0;
	}
	//Upload CV
	function upload_cv_file($file,$id, $path, $formname, $allows_file)
	{

		$uploadfile = $path . $id."__".basename($file[$formname]['name']);

		$path_info = pathinfo($uploadfile);
		if(!in_array($path_info['extension'],$allows_file["EXT"]))
		    return 1;
		if($file[$formname]['size']>$allows_file["SIZE"][0])
			return 2;
		if (move_uploaded_file($file[$formname]['tmp_name'], $uploadfile))
		{
			return $id."__".basename($file[$formname]['name']);
		}
		return 0;
	}
	function numf($num)
	{

	return number_format($num, 2, '.', ',');
	}

	function get_rnd_iv($iv_len)
	{
	    $iv = '';
	    while ($iv_len-- > 0) {
		$iv .= chr(mt_rand() & 0xff);
	    }
	    return $iv;
	}

	function md5_encrypt($plain_text, $password, $iv_len = 16)
	{

	    $plain_text .= "\x13";
	    $n = strlen($plain_text);
	    if ($n % 16) $plain_text .= str_repeat("\0", 16 - ($n % 16));
	    $i = 0;
	    $enc_text = $this->get_rnd_iv($iv_len);
	    $iv = substr($password ^ $enc_text, 0, 512);
	    while ($i < $n) {
		$block = substr($plain_text, $i, 16) ^ pack('H*', md5($iv));
		$enc_text .= $block;
		$iv = substr($block . $iv, 0, 512) ^ $password;
		$i += 16;
	    }
	    return base64_encode($enc_text);
	}

	function md5_decrypt($enc_text, $password, $iv_len = 16)
	{
	    $enc_text = base64_decode($enc_text);
	    $n = strlen($enc_text);
	    $i = $iv_len;
	    $plain_text = '';
	    $iv = substr($password ^ substr($enc_text, 0, $iv_len), 0, 512);
	    while ($i < $n) {
		$block = substr($enc_text, $i, 16);
		$plain_text .= $block ^ pack('H*', md5($iv));
		$iv = substr($block . $iv, 0, 512) ^ $password;
		$i += 16;
	    }
	    return preg_replace('/\\x13\\x00*$/', '', $plain_text);
	}

	function save($table_name, &$columns)
  {
    global $connected;
    try {
      $sql = 'INSERT INTO ' . $table_name . '(' . implode(',', array_keys($columns)) . ')
            VALUES(:' . implode(',:', array_keys($columns)) . ')';
      $stm = $connected->prepare($sql);
      if ($stm == false) throw new Exception('Error sql statement');

      foreach ($columns as $key => $value) {
        $stm->bindValue(':' . $key, $value, PDO::PARAM_STR);
      }
      $stm->execute();
      return $connected->lastInsertId();
    } catch (Exception $e) {
      echo $e->getMessage();
    }
  }

	/**
	   * @author: E-KHMER
	   * upload image
	   * @param $file $_FILES
	   * @param $id id in time()
	   * @param $path file path in server
	   * @param $control_name control name
	   * @return int|string
	   */
	  function uploadFile($file, $id, $path, $control_name)
	  {
	    $upload_file = $path . $id."__".basename($file[$control_name]['name']);
	    $path_info = pathinfo($upload_file);
	    if (move_uploaded_file($file[$control_name]['tmp_name'], $upload_file))
	    {
	      chmod ($upload_file, 0755);
	      return $id . "__" . basename ($file[$control_name]['name']);
	    }

	    return 0;
	  }
}
?>
