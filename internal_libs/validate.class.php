<?php
    $f_name = $l_name = $email = $address = $phone_num = "";
    if($_SERVER["REQUEST_METHOD"] == "POST")
    {
        if(empty($_POST['First_Name']))
        {
            $f_name = "First Name is required";
        }
        else
        {
            $f_name = test_input($_POST['First_Name']);
        }
        if(empty($_POST['Last_Name']))
        {
            $l_name = "Last Name is required"; 
        }
        else
        {
            $l_name = test_input($_POST['Last_Name']);
        }
        if(empty($_POST['E_mail']))
        {
            $email = "E_mail is required";
        }
        else
        {
            $email = test_input($_POST['E_mail']);
        }
        if(empty($_POST['Address']))
        {
            $address = "Address is required";
        }
        else
        {
            $address = test_input($_POST['Address']);
        }
        if(empty($_POST['Phone_num']))
        {
            $phone_num = "Phone number is required";
        }
        else
        {
            $phone_num = test_input($_POST['Phone_num']);
        }
    }
    function test_input($data)
    {
        $data = trim($data);
        $data = stripcslashes($data);
        $data = htmlspecialchars($data);
        return $data;
    }
?>